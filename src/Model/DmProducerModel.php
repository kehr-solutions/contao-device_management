<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Model;


use Contao\Date;
use Contao\Model;

/**
 * Reads and writes producer
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $title
 * @property string  $alias
 * @property string  $text
 * @property string  $singleSRC
 * @property string  $url
 * @property boolean $target
 * @property boolean $showProducts
 * @property string  $pageTitle
 * @property string  $description
 * @property boolean $published
 * @property string  $start
 * @property string  $stop
 *
 * @package KehrSolutions\DeviceManagementBundle\Model
 */
class DmProducerModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_dm_producer';


    /**
     * Find published producer by their ID or aliases
     *
     * @param mixed $varId      the numeric ID or the alias name
     * @param array $arrOptions An optional options array
     *
     * @return Model\Collection|DmProducerModel[]|DmProducerModel|null A collection of models or null if there are no producer
     */
    public static function findPublishedByIdOrAlias($varId, array $arrOptions = array())
    {
        $t          = static::$strTable;
        $arrColumns = !is_numeric($varId) ? ["$t.alias=?"] : ["$t.id=?"];

        if (isset($arrOptions['ignoreFePreview']) || !BE_USER_LOGGED_IN) {
            $time = Date::floorToMinute();

            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

        return static::findBy($arrColumns, $varId, $arrOptions);
    }
}