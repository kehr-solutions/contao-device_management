<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Model;


use Contao\Model;

/**
 * Class DmProductTypeModel
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $name
 * @property string  $details
 * @property string  $list
 * @property string  $filter
 * @property string  $sorting
 *
 * @package KehrSolutions\DeviceManagementBundle\Model
 */
class DmProductTypeModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_dm_producttype';
}