<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Model;


use Contao\Date;
use Contao\Model;

/**
 * Class DmProductModel
 *
 * @property integer $id
 * @property string  $name
 * @property string  $alias
 * @property string  $images
 * @property string  $orderImages
 * @property string  $size
 * @property string  $pageTitle
 *
 * @package KehrSolutions\DeviceManagementBundle\Model
 */
class DmProductModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_dm_product';

    /**
     * Find published products by condition
     *
     * @param mixed $arrColumns
     * @param mixed $arrValues
     * @param array $arrOptions
     *
     * @return Model\Collection|DmProductModel|DmProductModel[]|null
     */
    public static function findPublishedBy($arrColumns, $arrValues, array $arrOptions = array())
    {
        $t = static::$strTable;

        $arrValues = (array) $arrValues;

        if (!is_array($arrColumns)) {
            $arrColumns = [static::$strTable . '.' . $arrColumns . '=?'];
        }

        // Add publish check to $arrColumn as the first item to enable SQL keys
        if (!BE_USER_LOGGED_IN) {
            $time = Date::floorToMinute();
            array_unshift(
                $arrColumns,
                "$t.published='1' AND ($t.start='' OR $t.start<'$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "')"
            );
        }

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }

    /**
     * Find a single product by its ID or alias
     *
     * @param mixed $varId      THe ID or alias
     * @param array $arrOptions An optional options array
     *
     * @return static|null The model or null if the result is empty
     */
    public static function findPublishedByIdOrAlias($varId, array $arrOptions = array())
    {
        $t = static::$strTable;

        $arrColumns = ["($t.id=? OR $t.alias=?)"];
        $arrValues  = [is_numeric($varId) ? $varId : 0, $varId];

        $arrOptions = array_merge(
            [
                'limit'  => 1,
                'return' => 'Model'
            ],
            $arrOptions
        );

        return static::findPublishedBy($arrColumns, $arrValues, $arrOptions);
    }


    public static function findPublishedByProducer($id, array $arrOptions = array())
    {
        $t          = static::$strTable;
        $arrColumns = ["$t.producerId=?"];

        if (isset($arrOptions['ignoreFePreview']) || !BE_USER_LOGGED_IN) {
            $time = Date::floorToMinute();

            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
        }

        return static::findBy($arrColumns, $id, $arrOptions);
    }
}



/*
 * Find published producer by their ID or aliases
 *
 * @param mixed $varId      the numeric ID or the alias name
 * @param array $arrOptions An optional options array
 *
 * @return Model\Collection|DmProducerModel[]|DmProducerModel|null A collection of models or null if there are no producer
 *
public static function findPublishedByIdOrAlias($varId, array $arrOptions = array())
{
    $t          = static::$strTable;
    $arrColumns = !is_numeric($varId) ? ["$t.alias=?"] : ["$t.id=?"];

    if (isset($arrOptions['ignoreFePreview']) || !BE_USER_LOGGED_IN) {
        $time = Date::floorToMinute();

        $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1'";
    }

    return static::findBy($arrColumns, $varId, $arrOptions);
}*/