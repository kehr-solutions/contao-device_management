<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\DataContainer;


use Contao\Automator;
use Contao\Controller;
use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;
use Doctrine\DBAL\Connection;
use KehrSolutions\DeviceManagementBundle\Model\DmProducerModel;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class DmProducer implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * DmProducer constructor.
     *
     * @param Connection       $db
     * @param SessionInterface $session
     */
    public function __construct(Connection $db, SessionInterface $session)
    {
        $this->db      = $db;
        $this->session = $session;
    }

    /**
     * Auto-generate the producer alias if it has not been set yet
     *
     * @param mixed         $varValue
     * @param DataContainer $dc
     *
     * @return string
     */
    public function onGenerateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if ($varValue == '') {
            $autoAlias   = true;
            $slugOptions = [
                'locale'     => 'de',
                'validChars' => '0-9a-z'
            ];

            $varValue = System::getContainer()->get('contao.slug.generator')->generate(StringUtil::stripInsertTags($dc->activeRecord->title), $slugOptions);
        }

        $exists = $this->db->fetchColumn("SELECT id FROM {$dc->table} WHERE alias=? AND id!=?", [$varValue, $dc->activeRecord->id]);

        // Check whether the producer alis exists
        if ($exists) {
            if (!$autoAlias) {
                throw new \RuntimeException(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Return the delete producer button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function onDeleteProducer($row, $href, $label, $title, $icon, $attributes)
    {
        /** @var Image|object $imageAdapter */
        $imageAdapter = $this->framework->getAdapter(Image::class);

        /** @var Controller|object $controller */
        $controller = $this->framework->getAdapter(Controller::class);

        $exists = $this->db->fetchColumn("SELECT id FROM tl_dm_product WHERE producerId=?", [$row['id']]);

        return $exists ? $imageAdapter->getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ' : '<a href="' . $controller->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . $imageAdapter->getHtml($icon, $label) . '</a> ';
    }

    /**
     * Automatically generate the producer URL aliases
     *
     * @param array $arrButtons
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function onAddAliasButton($arrButtons)
    {
        /** @var Input|object $input */
        $input = $this->framework->getAdapter(Input::class);

        // Generate the aliases
        if ($input->post('FORM_SUBMIT') == 'tl_select' && isset($_POST['alias'])) {
            $session = $this->session->all();
            $ids     = $session['CURRENT']['IDS'];

            foreach ($ids as $id) {
                $objProducer = DmProducerModel::findByPk($id);

                if ($objProducer === null) {
                    continue;
                }

                // Set the new alias
                $slugOptions = [
                    'locale'     => 'de',
                    'validChars' => '0-9a-z'
                ];

                $strAlias = System::getContainer()->get('contao.slug.generator')->generate(StringUtil::stripInsertTags($objProducer->title), $slugOptions);

                // The alias has not changed
                if ($strAlias == $objProducer->alias) {
                    continue;
                }

                // Initialize the version manager
                $objVersions = new Versions('tl_dm_producer', $id);
                $objVersions->initialize();

                // Store the new alias
                $this->db->prepare("UPDATE tl_dm_producer SET alias=? WHERE id=?")
                    ->execute([$strAlias, $id]);
            }

            /** @var Controller|object $controller */
            $controller = $this->framework->getAdapter(Controller::class);
            $controller->redirect($controller->getReferer());
        }

        // Add the button
        $arrButtons['alias'] = '<button type="submit" name="alias" id="alias" class="tl_submit" accesskey="e">' . $GLOBALS['TL_LANG']['MSC']['aliasSelected'] . '</button> ';


        return $arrButtons;
    }

    /**
     * Schedule a dm producer update
     *
     * @param DataContainer $dc
     */
    public function onScheduleUpdate(DataContainer $dc)
    {
        /** @var Input|object $input */
        $input = $this->framework->getAdapter(Input::class);

        // Return if there is no ID
        if (!$dc->activeRecord || $input->get('act') == 'copy') {
            return;
        }

        // Store the ID in the session
        $session   = $this->session->get('dm_producer_updater');
        $session[] = $dc->activeRecord->id;
        $this->session->set('dm_producer_updater', array_unique($session));
    }

    /**
     * Check for modified producer and update the XML files if necessary.
     */
    public function onGenerateSitemap()
    {
        $session = $this->session->get('dm_producer_updater');

        if (!is_array($session) || empty($session)) {
            return;
        }

        $objAutomater = new Automator();
        $objAutomater->generateSitemap();

        $this->session->set('dm_producer_updater', null);
    }
}