<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\DataContainer;


use Contao\Automator;
use Contao\Controller;
use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\DataContainer;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;
use Doctrine\DBAL\Connection;
use KehrSolutions\DeviceManagementBundle\Model\DmProductModel;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class DmProduct implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * DmProducer constructor.
     *
     * @param Connection       $db
     * @param SessionInterface $session
     */
    public function __construct(Connection $db, SessionInterface $session)
    {
        $this->db      = $db;
        $this->session = $session;
    }

    /**
     * Auto-generate the product alias if it has not been set yet
     *
     * @param mixed         $varValue
     * @param DataContainer $dc
     *
     * @return string
     */
    public function onGenerateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate alias if there is none
        if ($varValue == '') {
            $autoAlias   = true;
            $slugOptions = [
                'locale'     => 'de',
                'validChars' => '0-9a-z'
            ];

            $varValue = System::getContainer()->get('contao.slug.generator')->generate(StringUtil::stripInsertTags($dc->activeRecord->name), $slugOptions);
        }

        $exists = $this->db->fetchColumn("SELECT id FROM {$dc->table} WHERE alias=? AND id!=?", [$varValue, $dc->activeRecord->id]);

        // Check whether the producer alis exists
        if ($exists) {
            if (!$autoAlias) {
                throw new \RuntimeException(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Automatically generate the product URL aliases
     *
     * @param array $arrButtons
     *
     * @return array
     */
    public function onAddAliasButton($arrButtons)
    {
        /** @var Input|object $input */
        $input = $this->framework->getAdapter(Input::class);

        // Generate the aliases
        if ($input->post('FORM_SUBMIT') == 'tl_select' && isset($_POST['alias'])) {
            $session = $this->session->all();
            $ids     = $session['CURRENT']['IDS'];

            foreach ($ids as $id) {
                $objProducer = DmProductModel::findByPk($id);

                if ($objProducer === null) {
                    continue;
                }

                // Set the new alias
                $slugOptions = [
                    'locale'     => 'de',
                    'validChars' => '0-9a-z'
                ];

                $strAlias = System::getContainer()->get('contao.slug.generator')->generate(StringUtil::stripInsertTags($objProducer->name), $slugOptions);

                // The alias has not changed
                if ($strAlias == $objProducer->alias) {
                    continue;
                }

                // Initialize the version manager
                $objVersions = new Versions('tl_dm_producer', $id);
                $objVersions->initialize();

                // Store the new alias
                $this->db->prepare("UPDATE tl_dm_product SET alias=? WHERE id=?")
                    ->execute([$strAlias, $id]);
            }

            /** @var Controller|object $controller */
            $controller = $this->framework->getAdapter(Controller::class);
            $controller->redirect($controller->getReferer());
        }

        // Add the button
        $arrButtons['alias'] = '<button type="submit" name="alias" id="alias" class="tl_submit" accesskey="e">' . $GLOBALS['TL_LANG']['MSC']['aliasSelected'] . '</button> ';


        return $arrButtons;
    }

    public function onCreate($strTable, $insertId, $arrSet)
    {
        $this->db->executeQuery("UPDATE {$strTable} SET dateAdded=? WHERE id=?", [time(), $insertId]);
    }

    /**
     * @param DataContainer $dc
     *
     */
    public function onLoadCallback(DataContainer $dc)
    {
        $arrFields = [];

        $arrDetails = $this->db->executeQuery(
            "SELECT
                  pt.details
                FROM
                  tl_dm_product_category pc
                INNER JOIN tl_page p ON p.id=pc.page_id
                INNER JOIN tl_dm_producttype pt ON p.dmProductType=pt.id
                WHERE pc.product_id=?",
            [$dc->id]
        )->fetchAll();


        if (!empty($arrDetails)) {
            foreach ($arrDetails as $details) {
                $arrFields = array_unique(array_merge($arrFields, StringUtil::deserialize($details['details'], true)));
            }
        }

        if (!empty($arrFields)) {
            foreach ($arrFields as $field) {
                $legend = $GLOBALS['TL_DCA'][$dc->table]['fields'][$field]['attributes']['legend'];

                PaletteManipulator::create()
                    ->addLegend($legend, 'sheet_legend', PaletteManipulator::POSITION_BEFORE)
                    ->addField($field, $legend, PaletteManipulator::POSITION_APPEND)
                    ->applyToPalette('default', 'tl_dm_product');
            }
        }
    }

    /**
     * @param DataContainer $dc
     */
    public function scheduleUpdate(DataContainer $dc)
    {
        // Return if there is no ID
        if (!$dc->id || Input::get('act') == 'copy') {
            return;
        }

        /** @var SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        // Store the ID in the session
        $session   = $objSession->get('dm_product_updater');
        $session[] = $dc->id;

        $objSession->set('dm_product_updater', array_unique($session));
    }

    public function generateXml()
    {
        /** @var SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        $session = $objSession->get('dm_product_updater');

        if (empty($session) || !is_array($session)) {
            return;
        }

        $objAutomator = new Automator();
        $objAutomator->generateSitemap();

        $objSession->set('dm_product_updater', null);
    }
}