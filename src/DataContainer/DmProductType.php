<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\DataContainer;


use Contao\Controller;
use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\DataContainer;
use Contao\StringUtil;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class DmProductType implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * DmProducer constructor.
     *
     * @param Connection       $db
     * @param SessionInterface $session
     */
    public function __construct(Connection $db, SessionInterface $session)
    {
        $this->db      = $db;
        $this->session = $session;
    }

    public function onGetDetailsFields()
    {
        $arrOptions = [];
        Controller::loadDataContainer('tl_dm_product');
        Controller::loadLanguageFile('tl_dm_product');

        foreach ($GLOBALS['TL_DCA']['tl_dm_product']['fields'] as $name => $field) {
            if (!$field['attributes']['beEdit']) {
                continue;
            }

            $arrOptions[$name] = $GLOBALS['TL_LANG']['tl_dm_product'][$name][0];
        }

        return $arrOptions;
    }

    public function onGetListFields(DataContainer $dc)
    {
        $arrOptions = [];
        $arrFields  = StringUtil::deserialize($dc->activeRecord->details, true);

        Controller::loadLanguageFile('tl_dm_product');

        foreach ($arrFields as $field) {
            $arrOptions[$field] = $GLOBALS['TL_LANG']['tl_dm_product'][$field][0];
        }


        return $arrOptions;
    }

    public function onGetFilterFields(DataContainer $dc)
    {
        $arrOptions = [];
        $arrFields  = StringUtil::deserialize($dc->activeRecord->details, true);

        Controller::loadLanguageFile('tl_dm_product');
        Controller::loadDataContainer('tl_dm_product');

        foreach ($arrFields as $field) {
            if (!$GLOBALS['TL_DCA']['tl_dm_product']['fields'][$field]['attributes']['feFilter']) {
                continue;
            }

            $arrOptions[$field] = $GLOBALS['TL_LANG']['tl_dm_product'][$field][0];
        }

        foreach ($GLOBALS['TL_DCA']['tl_dm_product']['fields'] as $name => $field) {
            if ($field['attributes']['systemColumn'] && $field['attributes']['feFilter']) {
                $arrOptions[$name] = $GLOBALS['TL_LANG']['tl_dm_product'][$name][0];
            }
        }

        return $arrOptions;
    }

    public function onGetSortingFields(DataContainer $dc)
    {
        $arrOptions = [];
        $arrFields  = StringUtil::deserialize($dc->activeRecord->details, true);

        Controller::loadLanguageFile('tl_dm_product');
        Controller::loadDataContainer('tl_dm_product');

        foreach ($arrFields as $field) {
            if (!$GLOBALS['TL_DCA']['tl_dm_product']['fields'][$field]['attributes']['feSorting']) {
                continue;
            }

            $arrOptions[$field] = $GLOBALS['TL_LANG']['tl_dm_product'][$field][0];
        }

        foreach ($GLOBALS['TL_DCA']['tl_dm_product']['fields'] as $name => $field) {
            if ($field['attributes']['systemColumn'] && $field['attributes']['feSorting']) {
                $arrOptions[$name] = $GLOBALS['TL_LANG']['tl_dm_product'][$name][0];
            }
        }

        return $arrOptions;
    }
}