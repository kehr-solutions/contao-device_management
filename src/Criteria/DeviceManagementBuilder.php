<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Criteria;


use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Doctrine\DBAL\Connection;
use Nelmio\SecurityBundle\ContentSecurityPolicy\Violation\Exception\NoDataException;

class DeviceManagementBuilder implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * @var Connection
     */
    private $db;

    /**
     * DeviceManagementBuilder constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Get the criteria for product list module
     *
     * @param string $strView
     *
     * @return ProductCriteria|null
     */
    public function getCriteriaForProductList(string $strView = 'list')
    {
        $criteria = new ProductCriteria($this->framework, $this->db);

        try {
            $criteria->setBasicCriteria($strView);
        } catch (NoDataException $e) {
            return null;
        }

        return $criteria;
    }
}



/*
 * Get the criteria for list module.
 *
 * @param array     $archives
 * @param bool|null $featured
 * @param Module    $module
 *
 * @return NewsCriteria|null
 *
public function getCriteriaForListModule(array $archives, $featured, Module $module)
{
    $criteria = new NewsCriteria($this->framework);
    try {
        $criteria->setBasicCriteria($archives, $module->news_order);
        // Set the featured filter
        if (null !== $featured) {
            $criteria->setFeatured($featured);
        }
        // Set the criteria for related categories
        if ($module->news_relatedCategories) {
            $this->setRelatedListCriteria($criteria, $module);
        } else {
            // Set the regular list criteria
            $this->setRegularListCriteria($criteria, $module);
        }
    } catch (NoNewsException $e) {
        return null;
    }
    return $criteria;
}*/