<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Criteria;


use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\Date;
use Doctrine\DBAL\Connection;

class ProductCriteriaBuilder implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var null|mixed
     */
    private $idOrAlias;

    /** @var Connection */
    private $queryBuilder;

    /**
     * ProductCriteriaBuilder constructor.
     *
     * @param Connection $db
     * @param null       $idOrAlias
     */
    public function __construct(Connection $db, $idOrAlias = null)
    {
        $this->db           = $db;
        $this->idOrAlias    = $idOrAlias;
        $this->queryBuilder = $db->createQueryBuilder('p');
        $this->setDefaultFilter();

        if ($idOrAlias) {
            $this->queryBuilder->andWhere("id='$idOrAlias' OR alias='$idOrAlias'");
        }
    }

    /**
     * Set default filter for Products
     */
    private function setDefaultFilter()
    {
        $this->queryBuilder
            ->select('p.name, p.alias, p.pageTitle, p.description, p.teaser, p.text', 'p.sheets', 'p.images', 'p.orderImages')
            ->from('tl_dm_product', 'p');

        if (!BE_USER_LOGGED_IN) {
            $time = Date::floorToMinute();
            $this->queryBuilder->where("p.published='1'")
                ->andWhere("p.start='' OR p.start<='$time'")
                ->andWhere("p.stop='' OR p.stop>'" . ($time + 60) . "'");
        }
    }

    /**
     * @return string
     */
    public function getProductQuery()
    {
        return $this->queryBuilder->getSQL();
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        if ($this->idOrAlias) {
            return $this->queryBuilder->execute()->fetch();
        } else {
            return $this->queryBuilder->execute()->fetchAll();
        }
    }

    /**
     * Count the total matching products
     *
     * @return integer
     */
    public function getCountProducts()
    {
        return $this->queryBuilder->execute()->rowCount();
    }

    /**
     * @param $arrCategories
     */
    public function setProductCategories($arrCategories)
    {
        $this->queryBuilder->join("p", "tl_dm_product_category", "c", "c.product_id=p.id")
            ->andWhere($this->queryBuilder->expr()->in('c.page_id', $arrCategories));
    }

    /**
     * @param $intLimit
     */
    public function setLimit($intLimit)
    {
        $this->queryBuilder->setMaxResults($intLimit);
    }

    /**
     * @param $intOffset
     */
    public function setOffset($intOffset)
    {
        $this->queryBuilder->setFirstResult($intOffset);
    }

    public function setSelectFields($arrFields)
    {
        if (!empty($arrFields)) {
            $this->queryBuilder->addSelect($arrFields);
        }
    }

    public function setOrderBy($arrOrder)
    {
        if (!empty($arrOrder)) {
            foreach ($arrOrder as $order) {
                $this->queryBuilder->addOrderBy($order);
            }
        }
    }
}