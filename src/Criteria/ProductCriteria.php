<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

declare(strict_types=1);

namespace KehrSolutions\DeviceManagementBundle\Criteria;


use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\Date;
use Contao\PageModel;
use Contao\StringUtil;
use Doctrine\DBAL\Connection;
use KehrSolutions\DeviceManagementBundle\Model\DmProductTypeModel;

class ProductCriteria
{
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var array
     */
    private $attribute = [];

    /**
     * @var array
     */
    private $defaultSelectFields = [
        'list'    => "p.indoor, p.outdoor",
        'details' => "p.indoor, p.outdoor, p.orderImages, p.size, p.pageTitle, p.description, p.teaser, p.text, p.sheets, p.orderSheets, p.linkProducer, p.producerId, p.equipment, p.showForm"
    ];

    /**
     * @var \Doctrine\DBAL\Query\QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var string
     */
    private $viewMode;

    /**
     * ProductCriteria constructor.
     *
     * @param ContaoFrameworkInterface $framework
     */
    public function __construct(ContaoFrameworkInterface $framework, Connection $db)
    {
        $this->framework    = $framework;
        $this->db           = $db;
        $this->queryBuilder = $db->createQueryBuilder();
    }

    /**
     * Set the basic criteria
     *
     * @param string $strViewMode
     */
    public function setBasicCriteria(string $strViewMode = 'list')
    {
        $this->queryBuilder
            ->addSelect("p.id, p.name, p.alias, p.images, p.dateAdded")
            ->from("tl_dm_product", "p");

        $this->viewMode = $strViewMode;

        if (!BE_USER_LOGGED_IN) {
            /** @var Date|object $dataAdapter */
            $dataAdapter = $this->framework->getAdapter(Date::class);
            $time        = $dataAdapter->floorToMinute();

            $this->queryBuilder
                ->andWhere("p.published='1'")
                ->andWhere("p.start='' OR p.start<='$time'")
                ->andWhere("p.stop='' OR p.stop>'" . ($time + 60) . "'");
        }
    }

    /**
     * Set the limit
     *
     * @param $limit
     */
    public function setLimit($limit)
    {
        $this->queryBuilder
            ->setMaxResults($limit);
    }

    /**
     * Set the offset
     *
     * @param $offset
     */
    public function setOffset($offset)
    {
        $this->queryBuilder
            ->setFirstResult($offset);
    }

    /**
     * Set the product ID
     *
     * @param mixed $varId
     */
    public function setProduct($varId)
    {
        $this->queryBuilder
            ->andWhere(!is_numeric($varId) ? "p.alias='$varId'" : "p.id=$varId");
    }

    /**
     * Return the Query string
     *
     * @return string
     */
    public function getQueryString()
    {
        return $this->queryBuilder->getSQL();
    }

    /**
     * Return the total matching products
     *
     * @return int
     */
    public function countProducts()
    {
        return $this->queryBuilder->execute()->rowCount();
    }

    /**
     * Return all matching products
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->queryBuilder->execute()->fetchAll();
    }

    /**
     * Return the current product
     *
     * @return mixed
     */
    public function getProduct()
    {
        return $this->queryBuilder->execute()->fetch();
    }

    /**
     * Get select fields
     *
     * @param int $category
     *
     * @return array
     */
    private function getSelectFields(int $category)
    {
        $objPage = PageModel::findWithDetails($category);

        if (null === $objPage) {
            return ["p.*"];
        }

        $objProductType = DmProductTypeModel::findByPk($objPage->dmProductType);

        if (null === $objProductType) {
            return ["p.*"];
        }

        $mode = $this->viewMode;

        $arrFields = StringUtil::deserialize($objProductType->$mode, true);

        $arrSelects = [
            $this->defaultSelectFields[$mode]
        ];

        foreach ($arrFields as $field) {
            $this->attribute[] = $field;
            $arrSelects[]      = "p." . $field;
        }

        return $arrSelects;
    }

    /**
     * Return all attribute from the category or product
     *
     * @return array
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set order by
     *
     * @param string      $sort
     * @param string|null $order
     */
    public function setOrderBy(string $sort, string $order = null)
    {
        $this->queryBuilder
            ->addOrderBy($sort, $order);
    }

    /**
     * Set sales settings
     */
    private function setSalesSettings()
    {
        $this->queryBuilder
            ->addSelect(["p.manufactured, p.operatingHours, p.addPrice, p.price"]);

        $this->attribute[] = 'manufactured';
        $this->attribute[] = 'operatingHours';
        $this->attribute[] = 'price';
    }

    /**
     * Set category
     *
     * @param mixed $category
     */
    public function setCategory($category)
    {
        if (is_array($category) || is_numeric($category)) {
            $this->queryBuilder
                ->join("p", "tl_dm_product_category", "c", "c.product_id=p.id");
        }

        if (is_array($category)) {
            $this->queryBuilder
                ->andWhere($this->queryBuilder->expr()->in("c.page_id", $category));
        }

        if (is_integer($category)) {
            $this->queryBuilder
                ->addSelect($this->getSelectFields($category))
                ->andWhere("c.page_id=" . $category);

            $objCategory = PageModel::findWithDetails($category);

            if ($objCategory !== null) {
                if ($objCategory->dmSale) {
                    $this->queryBuilder
                        ->andWhere("p.sale=" . $objCategory->dmSale);

                    $this->setSalesSettings();
                }

                if ($objCategory->dmRent) {
                    $this->queryBuilder
                        ->andWhere("p.rent=" . $objCategory->dmRent);
                }
            }
        }
    }
}