<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

declare(strict_types=1);

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\ContentAccordionStart;
use Contao\ContentAccordionStop;
use Contao\ContentDownloads;
use Contao\ContentGallery;
use Contao\ContentImage;
use Contao\ContentModel;
use Contao\Controller;
use Contao\Date;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\Model\Collection;
use Contao\Module;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use KehrSolutions\DeviceManagementBundle\Model\DmProducerModel;

/**
 * Class DmModule
 *
 * @property string  $dm_template
 * @property boolean $dmSetJumpTo
 * @property integer $dmJumpTo
 * @property string  $dmDefaultImage
 * @property integer $sliderInterval
 * @property boolean $showSliderControls
 * @property boolean $showThumb
 * @property integer $dmRootPage
 * @property integer $dmRequestForm
 * @property boolean $dmSetNew
 * @property integer $dmNewDays
 * @property boolean $dmShowProducerWebsite
 *
 * @package KehrSolutions\DeviceManagementBundle\Module
 */
abstract class DmModule extends Module
{
    /**
     * Parse one or more items and return them as array
     *
     * @param array $products
     * @param array $attribute
     *
     * @return array
     * @throws \Exception
     */
    protected function parseProducts(array $products, array $attribute = array())
    {
        $limit = count($products);

        if ($limit < 1) {
            return [];
        }

        $count       = 0;
        $arrProducts = [];

        foreach ($products as $product) {
            $arrProducts[] = $this->parseProduct($product, $attribute, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count);
        }

        return $arrProducts;
    }

    /**
     * Parse an item and return it as string
     *
     * @param array  $arrProduct
     * @param string $strClass
     * @param int    $intCount
     * @param array  $attribute
     *
     * @return string
     * @throws \Exception
     */
    protected function parseProduct(array $arrProduct, array $attribute, string $strClass = '', int $intCount = 0)
    {
        /** @var PageModel $objPage */
        global $objPage;

        $objCategory = null;

        if (isset($arrProduct['categoryId']) && $arrProduct['categoryId'] !== 0) {
            $objCategory = PageModel::findWithDetails($arrProduct['categoryId']);
        }

        $arrAttributes = [];

        /** @var FrontendTemplate|object $objTemplate */
        $objTemplate = new FrontendTemplate($this->dm_template);
        $objTemplate->setData($arrProduct);

        $objTemplate->class = trim($strClass);

        // Link to producer
        if ($arrProduct['linkProducer'] && $this->dmSetJumpTo) {
            $objDmProducer   = DmProducerModel::findByPk($arrProduct['producerId']);
            $objProducerJump = PageModel::findWithDetails($this->dmJumpTo);

            if ($objDmProducer !== null && $objProducerJump !== null) {
                $objTemplate->producerLink = $this->generateLink(
                    $objProducerJump->alias . '/{producer}',
                    [
                        'producer'  => $objDmProducer->alias,
                        'auto_item' => 'producer'
                    ],
                    $objDmProducer->pageTitle ?: $objDmProducer->title,
                    sprintf($GLOBALS['TL_LANG']['MSC']['moreProducts'], $objDmProducer->title)
                );
            }
        }

        // Generate product alias
        if ($objPage->dmSetReaderJumpTo || $objCategory->dmSetReaderJumpTo) {
            $strPageAlias = $objCategory->alias ?: $objPage->alias;

            if (($objReaderPage = $objPage->getRelated('dmReaderJumpTo')) !== null || ($objReaderPage = $objCategory->getRelated('dmReaderJumpTo')) !== null) {
                $strPageAlias = $objReaderPage->alias;
            }

            $objTemplate->more = $this->generateLink(
                $strPageAlias . '/{product}',
                [
                    'product'   => $arrProduct['alias'],
                    'auto_item' => 'product',
                    'c'         => $objCategory->id ?: $objPage->id
                ],
                sprintf($GLOBALS['TL_LANG']['MSC']['readMoreProducts'], $arrProduct['name']),
                $GLOBALS['TL_LANG']['MSC']['dmReadMore']
            );

            $objTemplate->headline = $this->generateLink(
                $strPageAlias . '/{product}',
                [
                    'product'   => $arrProduct['alias'],
                    'auto_item' => 'product',
                    'c'         => $objCategory->id ?: $objPage->id
                ],
                $arrProduct['name'],
                $arrProduct['name']
            );
        }

        $objTemplate->description = StringUtil::toHtml5($arrProduct['text']);

        // Category-Name
        if (Input::get('c') && ($categoryName = PageModel::findPublishedById(Input::get('c'))) !== null) {
            $objTemplate->categoryName = $categoryName->title;
        }

        // Convert equipment text to html5
        if (isset($arrProduct['equipment'])) {
            $objTemplate->equipment = $this->generateAccordionStart($GLOBALS['TL_LANG']['MSC']['equipment']) . StringUtil::toHtml5($arrProduct['equipment']) . $this->generateAccordionStop();
        }

        $objTemplate->sheets = $this->generateDownloads(StringUtil::deserialize($arrProduct['sheets'], true), $arrProduct['orderSheets']);

        // Generate product images
        $objTemplate->image = $this->generateImages(StringUtil::deserialize($arrProduct['images'], true), $arrProduct['orderImages'], $arrProduct['size']);

        // Generate request form
        if ($arrProduct['showForm'] && $this->dmRequestForm) {
            $objForm = Controller::getForm($this->dmRequestForm);

            $objTemplate->requestForm = $this->generateAccordionStart($GLOBALS['TL_LANG']['MSC']['requestForm']) . $objForm . $this->generateAccordionStop();
        }

        if (!empty($attribute)) {
            Controller::loadDataContainer('tl_dm_product');
            Controller::loadLanguageFile('tl_dm_product');

            foreach ($attribute as $field) {
                if (is_numeric($arrProduct[$field]) && $arrProduct[$field] == 0 && $field !== 'price') {
                    continue;
                } elseif ($arrProduct[$field] == '' && $field !== 'price') {
                    continue;
                }

                $value = $arrProduct[$field];
                $unit  = &$GLOBALS['TL_LANG']['tl_dm_product'][$field][2] ?: '';


                switch ($GLOBALS['TL_DCA']['tl_dm_product']['fields'][$field]['attributes']['fieldType']) {
                    case 'date':
                        $value = Date::parse($objPage->dateFormat, $arrProduct[$field]);
                        break;
                    case 'decimal':
                        $value = System::getFormattedNumber($arrProduct[$field]);
                        break;
                    case 'select':
                        $value = $GLOBALS['TL_LANG']['tl_dm_product'][$arrProduct[$field]];
                        break;
                }

                if (isset($arrProduct['price']) && !$arrProduct['addPrice'] && $field === 'price') {
                    $value = $GLOBALS['TL_LANG']['MSC']['noPrice'];
                    $unit  = '';
                }

                $arrAttributes[] = [
                    'name'  => &$GLOBALS['TL_LANG']['tl_dm_product'][$field][0],
                    'value' => $value,
                    'unit'  => $unit
                ];
            }

            $objTemplate->attributes = $arrAttributes;
        }


        if ($this->dmSetNew && $this->dmNewDays > 0) {
            $time = Date::floorToMinute();

            if ($time < ($arrProduct['dateAdded'] + (86400 * $this->dmNewDays))) {
                $objTemplate->new = $GLOBALS['TL_LANG']['MSC']['labelNew'];
            }
        }

        $objTemplate->tooltips = $GLOBALS['TL_LANG']['MSC']['tooltips'];

        return $objTemplate->parse();
    }

    /**
     * @param $arrDownloads
     * @param $orderSRC
     *
     * @return string
     */
    protected function generateDownloads($arrDownloads, $orderSRC): string
    {

        if (!is_array($arrDownloads) || empty($arrDownloads)) {
            return '';
        }

        $objContentModel = new ContentModel();

        $objContentModel->multiSRC  = $arrDownloads;
        $objContentModel->type      = 'downloads';
        $objContentModel->customTpl = 'ce_downloads_product';
        $objContentModel->sortBy    = 'custom';
        $objContentModel->orderSRC  = $orderSRC;

        $objDownloads = new ContentDownloads($objContentModel);

        return ($this->generateAccordionStart($GLOBALS['TL_LANG']['MSC']['dataSheets']) . $objDownloads->generate() . $this->generateAccordionStop());
    }

    /**
     * Generate the images
     *
     * @param array       $images
     * @param string|null $orderSRC
     * @param string|null $size
     *
     * @return string
     */
    protected function generateImages(array $images, $orderSRC, $size = ''): string
    {
        $objContentModel = new ContentModel();

        if (!$images) {
            $images = StringUtil::deserialize($this->dmDefaultImage, true);
        }

        $objContentModel->size =  $this->imgSize;

        if (!$orderSRC) {
            $objContentModel->type      = 'image';
            $objContentModel->singleSRC = $images[0];

            $objImage = new ContentImage($objContentModel);
        } else {
            $objContentModel->type               = 'gallery';
            $objContentModel->useHomeDir         = false;
            $objContentModel->sortBy             = 'custom';
            $objContentModel->perRow             = 1;
            $objContentModel->multiSRC           = $images;
            $objContentModel->orderSRC           = $orderSRC;
            $objContentModel->fullsize           = true;
            $objContentModel->galleryTpl         = 'ce_gallery_product';
            $objContentModel->sliderInterval     = $this->sliderInterval;
            $objContentModel->showSliderControls = $this->showSliderControls;
            $objContentModel->showThumb          = $this->showThumb;
            $objContentModel->id                 = $this->id;

            $objImage = new ContentGallery($objContentModel);
        }

        return $objImage->generate();
    }

    /**
     * Generate accordion start element
     *
     * @param string $strLabel
     *
     * @return string
     */
    private function generateAccordionStart(string $strLabel = ''): string
    {
        $objContentModel              = new ContentModel();
        $objContentModel->type        = 'accordionStart';
        $objContentModel->mooHeadline = $strLabel;

        $objAccordionStart = new ContentAccordionStart($objContentModel);

        return $objAccordionStart->generate();
    }

    /**
     * Generate accordion end element
     *
     * @return string
     */
    private function generateAccordionStop(): string
    {
        $objContentModel = new ContentModel();

        $objContentModel->type = 'accordionStop';

        $objAccordionStop = new ContentAccordionStop($objContentModel);

        return $objAccordionStop->generate();
    }

    /**
     * Parse an category and return it as string
     *
     * @param PageModel $objCategory
     * @param string    $strClass
     *
     * @return string
     */
    protected function parseCategory($objCategory, string $strClass = ''): string
    {
        /** @var FrontendTemplate|object $objTemplate */
        $objTemplate = new FrontendTemplate($this->dm_template);
        $objTemplate->setData($objCategory->row());

        if ($objCategory->cssClass != '') {
            $strClass = ' ' . $objCategory->cssClass . $strClass;
        }

        $objTemplate->class = trim($strClass);

        // Add an image
        $objModel = FilesModel::findByUuid($objCategory->dmImage ?: $this->dmDefaultImage);

        if ($objModel !== null && is_file(TL_ROOT . '/' . $objModel->path)) {
            $arrCategory = $objCategory->row();

            // Override the default size
            if ($this->imgSize != '') {
                $size = StringUtil::deserialize($this->imgSize);

                if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2])) {
                    $arrCategory['size'] = $this->imgSize;
                }
            }

            $arrCategory['singleSRC'] = $objModel->path;
            $this->addImageToTemplate($objTemplate, $arrCategory, null, null, $objModel);
        }

        // Generate the headline link
        $objTemplate->linkHeadline = $this->generateLink(
            $objCategory->alias,
            null,
            $objCategory->pageTitle ?: $objCategory->title,
            $objCategory->title
        );

        // Generate the more information button
        $objTemplate->more = $this->generateLink(
            $objCategory->alias,
            null,
            $objCategory->pageTitle ?: $objCategory->title,
            $GLOBALS['TL_LANG']['MSC']['dmReadMore']
        );

        return $objTemplate->parse();
    }

    /**
     * Parse one or more categories and return them as array
     *
     * @param Collection $objCategories
     *
     * @return array
     */
    protected function parseCategories($objCategories): array
    {
        $limit = $objCategories->count();

        if ($limit < 1) {
            return array();
        }

        $count         = 0;
        $arrCategories = [];

        while ($objCategories->next()) {
            /** @var PageModel|object $objCategory */
            $objCategory = $objCategories->current();

            if ($objCategory->hide) {
                continue;
            }

            $arrCategories[] = $this->parseCategory($objCategory, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'));
        }

        return $arrCategories;
    }

    /**
     * Generate a link and return it as string
     *
     * @param string     $alias
     * @param array|null $parameter
     * @param string     $linkTitle
     * @param string     $linkName
     *
     * @return string
     */
    protected function generateLink(string $alias, array $parameter = null, string $linkTitle, string $linkName): string
    {
        return sprintf(
            '<a href="%s" title="%s">%s</a>',
            $this->generateUrl($alias, $parameter),
            $linkTitle,
            $linkName
        );
    }

    /**
     * Generate a URL and return it as string
     *
     * @param string     $alias
     * @param array|null $parameter
     *
     * @return mixed
     */
    public function generateUrl(string $alias, array $parameter = null): string
    {
        $urlGenerator = System::getContainer()->get('contao.routing.url_generator');

        return $urlGenerator->generate($alias, $parameter);
    }
}