<?php

declare(strict_types=1);

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\BackendTemplate;
use Contao\Config;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Database;
use Contao\Environment;
use Contao\PageModel;
use Contao\StringUtil;
use Haste\Input\Input;
use KehrSolutions\DeviceManagementBundle\Model\DmProducerModel;
use KehrSolutions\DeviceManagementBundle\Model\DmProductModel;
use Patchwork\Utf8;

class ProducerReader extends DmModule
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_dm_producer_reader';

    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['dm_producer_reader'][0]) . ' ###';
            $objTemplate->title    = $this->headline;
            $objTemplate->id       = $this->id;
            $objTemplate->link     = $this->name;
            $objTemplate->href     = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['producer']) && Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            Input::setGet('producer', Input::get('auto_item'));
        }

        // Do not index or cache the page if no producer has been specified
        if (!Input::getAutoItem('producer', false, true)) {
            /** @var PageModel|object $objPage */
            global $objPage;

            $objPage->noSearch = 1;
            $objPage->cache    = 0;

            return '';
        }
        return parent::generate();
    }

    /**
     * Generate the module
     */
    protected function compile()
    {
        $this->Template->producer = '';
        $this->Template->referer  = 'javascript:history.go(-1)';
        $this->Template->back     = $GLOBALS['TL_LANG']['MSC']['goBack'];

        // Get the producer
        $objProducer = DmProducerModel::findPublishedByIdOrAlias(Input::getAutoItem('producer', false, true));

        if ($objProducer === null) {
            throw new PageNotFoundException('Page not found: ' . Environment::get('uri'));
        }

        $this->addMetaTags($objProducer);
        $producer = $objProducer->row();

        if ($objProducer->singleSRC) {
            $producer['image'] = $this->generateImages(StringUtil::deserialize($objProducer->singleSRC, true), null, null∆);
        }

        $this->Template->productsFrom     = $GLOBALS['TL_LANG']['MSC']['productsFrom'];
        $this->Template->goToWebsite      = $GLOBALS['TL_LANG']['MSC']['goToHomepage'];
        $this->Template->producer         = $producer;
        $this->Template->showProducerLink = $this->dmShowProducerWebsite;

        if ($objProducer->showProducts) {
            $this->Template->products = $this->getProductsFromProducer($objProducer->id);
        }

    }

    /**
     * Add meta header fields to the current page
     *
     * @param DmProducerModel $objProducer
     */
    private function addMetaTags($objProducer)
    {
        /** @var PageModel|object $objPage */
        global $objPage;

        $objPage->pageTitle   = $this->prepareMetaDescription($objProducer->pageTitle ?: $objProducer->title);
        $objPage->description = $this->prepareMetaDescription($objProducer->description ?: $objProducer->text);
    }

    /**
     * Get products by their producer
     *
     * @param $producer The producer ID
     *
     * @return string
     * @throws \Exception
     */
    private function getProductsFromProducer($producer)
    {
        $objProducts  = DmProductModel::findPublishedByProducer($producer);
        $strPageAlias = '';

        if ($objProducts === null) {
            return '';
        }

        $products = [];

        while ($objProducts->next()) {
            $objCategory = Database::getInstance()->prepare("SELECT page_id FROM tl_dm_product_category WHERE product_id=?")
                ->limit(1)
                ->execute($objProducts->id);

            if ($objCategory === null) {
                continue;
            }

            $objCategoryPage = PageModel::findPublishedById($objCategory->page_id);

            if ($objCategoryPage->dmSetReaderJumpTo) {
                $strPageAlias = $objCategoryPage->alias;

                if (($objReaderPage = $objCategoryPage->getRelated('dmReaderJumpTo')) !== null) {
                    $strPageAlias = $objReaderPage->alias;
                }
            }

            if (!$strPageAlias) {
                continue;
            }

            $products[$objProducts->id]             = $objProducts->row();
            $products[$objProducts->id]['image']    = $this->generateImages(StringUtil::deserialize($objProducts->images ?: $this->dmDefaultImage, true), null, $this->imgSize);
            $products[$objProducts->id]['more']     = $this->generateLink(
                $strPageAlias . '/{product}',
                [
                    'product'   => $objProducts->alias,
                    'auto_item' => 'product',
                    'c'         => $objCategory->page_id
                ],
                sprintf($GLOBALS['TL_LANG']['MSC']['readMoreProducts'], $objProducts->name),
                $GLOBALS['TL_LANG']['MSC']['dmReadMore']
            );
            $products[$objProducts->id]['headline'] = $this->generateLink(
                $strPageAlias . '/{product}',
                [
                    'product'   => $objProducts->alias,
                    'auto_item' => 'product',
                    'c'         => $objCategory->page_id
                ],
                $objProducts->name,
                $objProducts->name
            );
        }

        return !empty($products) ? $products : '';
    }
}