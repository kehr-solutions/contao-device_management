<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\BackendTemplate;
use Contao\PageModel;
use Haste\Form\Form;
use Haste\Input\Input;
use Patchwork\Utf8;

class ProductFilter extends DmModule
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_dm_product_filter';

    /**
     * @var integer
     */
    private $pageId;

    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['dmProductFilter'][0]) . ' ###';
            $objTemplate->title    = $this->headline;
            $objTemplate->id       = $this->id;
            $objTemplate->link     = $this->name;
            $objTemplate->href     = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        /** @var PageModel|object $objPage */
        global $objPage;

        if ($objPage->isProductPage && $objPage->dmProductType !== 0) {
            $this->pageId = $objPage->id;
        }

        if (!$this->pageId && Input::get('c') !== null) {
            $this->pageId = Input::get('c');
        }

        if (!$this->pageId && ($objResultPage = PageModel::findPublishedById($this->pageId)) === null) {
            return '';
        }

        return parent::generate();
    }

    /**
     * Generate the module
     */
    protected function compile()
    {
        $objPage = PageModel::findPublishedById($this->pageId);

        $objForm = new Form(
            'filter_' . $this->id,
            'GET',
            function () {
                return \Haste\Input\Input::get('foo') === 'bar';
            }
        );

        $objForm->setFormActionFromUri($objPage->getAbsoluteUrl());

        $objForm->addFormField(
            'test',
            array(
                'label'     => 'Test',
                'inputType' => 'text',
                'eval'      => array('mandatory' => true)
            )
        );

        $objForm->addFormField(
            'submit',
            [
                'label'     => 'Filtern',
                'inputType' => 'submit'
            ]
        );

        $this->Template->form = $objForm->generate();
    }
}