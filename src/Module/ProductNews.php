<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\BackendTemplate;
use Contao\PageModel;
use Contao\System;
use KehrSolutions\DeviceManagementBundle\Criteria\ProductCriteria;
use KehrSolutions\DeviceManagementBundle\Model\DmProductCategoryModel;
use Patchwork\Utf8;

class ProductNews extends DmModule
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_dm_product_news';

    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['dmProductNews'][0]) . ' ###';
            $objTemplate->title    = $this->headline;
            $objTemplate->id       = $this->id;
            $objTemplate->link     = $this->name;
            $objTemplate->href     = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        if (!$this->numberOfItems) {
            return '';
        }

        return parent::generate();
    }

    /**
     * Generate the module
     */
    protected function compile()
    {
        $GLOBALS['TL_CSS'][] = 'bundles/rocksolidslider/css/rocksolid-slider.min.css';
        $GLOBALS['TL_CSS'][] = 'bundles/rocksolidslider/css/default-skin.min.css';
        $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/rocksolidslider/js/rocksolid-slider.min.js|static';

        $this->Template->products = $this->parseProducts($this->fetchProductItems());
    }

    /**
     * Get the search criteria
     *
     * @return ProductCriteria|null
     */
    protected function getSearchCriteria()
    {
        return System::getContainer()
            ->get('kehr_solutions.device_management.criteria.dm_builder')
            ->getCriteriaForProductList();
    }

    protected function fetchProductItems()
    {
        $criteria = $this->getSearchCriteria();
        $criteria->setOrderBy("p.id", "DESC");
        $criteria->setLimit($this->numberOfItems);

        if ($criteria === null) {
            return null;
        }

        $products = $criteria->getProducts();

        foreach ($products as $key => $product) {
            $category = DmProductCategoryModel::findOneBy('product_id', $product['id']);

            if ($category === null || ($objPage = PageModel::findPublishedById((int) $category->row()['page_id'])) === null) {
                unset($products[$key]);
                continue;
            }

            $products[$key]['categoryId'] = $objPage->id;
        }

        return $products;
    }
}