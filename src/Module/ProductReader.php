<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

declare(strict_types=1);

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\BackendTemplate;
use Contao\Config;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Environment;
use Contao\PageModel;
use Contao\System;
use Haste\Input\Input;
use KehrSolutions\DeviceManagementBundle\Criteria\ProductCriteria;
use Patchwork\Utf8;

class ProductReader extends DmModule
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_dm_product_reader';

    /**
     * @var array
     */
    private $attribute = [];

    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['dmProductReader'][0]) . ' ###';
            $objTemplate->title    = $this->headline;
            $objTemplate->id       = $this->id;
            $objTemplate->link     = $this->name;
            $objTemplate->href     = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['product']) && Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            Input::setGet('product', Input::get('auto_item'));
        }

        // Do not index or cache the page if no product has been specified
        if (!Input::getAutoItem('product', false, true)) {
            /** @var PageModel|object $objPage */
            global $objPage;

            $objPage->noSearch = 1;
            $objPage->cache    = 0;

            return '';
        }

        return parent::generate();
    }

    /**
     * Generate the module
     */
    protected function compile()
    {
        $this->Template->products = '';
        $this->Template->referer  = 'javascript:history.go(-1)';
        $this->Template->back     = $GLOBALS['TL_LANG']['MSC']['goBack'];

        // Get the product
        $arrProduct = $this->fetchProduct((int) Input::get('c'));

        if (!is_array($arrProduct)) {
            throw new PageNotFoundException('Page not found: ' . Environment::get('uri'));
        }

        $this->addMetaTags($arrProduct);

        $this->Template->products = $this->parseProduct($arrProduct, $this->attribute);
    }

    /**
     * Add meta header fields to the current page
     *
     * @param array $arrProduct
     */
    protected function addMetaTags(array $arrProduct)
    {
        /** @var PageModel|object $objPage */
        global $objPage;

        $descriptionFallback = ($arrProduct['teaser'] ?: $arrProduct['text']);

        $objPage->pageTitle   = $this->prepareMetaDescription($arrProduct['pageTitle'] ?: $arrProduct['name']);
        $objPage->description = $this->prepareMetaDescription($arrProduct['description'] ?: $descriptionFallback);
    }

    /**
     * Get the search criteria
     *
     * @return ProductCriteria|null
     */
    protected function getSearchCriteria()
    {
        return System::getContainer()
            ->get('kehr_solutions.device_management.criteria.dm_builder')
            ->getCriteriaForProductList('details');
    }

    /**
     * Fetch the product
     *
     * @param int $intCategory The category ID
     *
     * @return null
     */
    protected function fetchProduct(int $intCategory)
    {
        $criteria = $this->getSearchCriteria();
        $criteria->setCategory($intCategory);

        if ($criteria === null) {
            return null;
        }

        $criteria->setProduct(Input::getAutoItem('product', false, true));

        $this->attribute = $criteria->getAttribute();

        return $criteria->getProduct();
    }
}