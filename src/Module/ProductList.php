<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

declare(strict_types=1);

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\BackendTemplate;
use Contao\Config;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\Environment;
use Contao\Input;
use Contao\PageModel;
use Contao\Pagination;
use Contao\StringUtil;
use Contao\System;
use KehrSolutions\DeviceManagementBundle\Criteria\ProductCriteria;
use KehrSolutions\DeviceManagementBundle\Model\DmProductTypeModel;
use Patchwork\Utf8;

class ProductList extends DmModule
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_dm_product_list';

    /**
     * @var array
     */
    private $attribute = [];

    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['dmProductList'][0]) . ' ###';
            $objTemplate->title    = $this->headline;
            $objTemplate->id       = $this->id;
            $objTemplate->link     = $this->name;
            $objTemplate->href     = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module
     */
    protected function compile()
    {
        /** @var PageModel|object $objPage */
        global $objPage;

        $limit  = null;
        $offset = (int) $this->skipFirst;

        // Maximum number of products
        if ($this->numberOfItems > 0) {
            $limit = $this->numberOfItems;
        }

        $this->Template->products = [];
        $this->Template->empty    = $GLOBALS['TL_LANG']['MSC']['emptyProductList'];

        // Get the total number of products
        $intTotal = $this->countProductItems((int) $objPage->id);

        if ($intTotal < 1) {
            return;
        }

        $total = $intTotal - $offset;

        // Split the results
        if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage)) {
            // Adjust the overall limit
            if (isset($limit)) {
                $total = min($limit, $total);
            }

            // Get the current page
            $id   = 'page_n' . $this->id;
            $page = (Input::get($id) !== null) ? Input::get($id) : 1;

            // Do not index or cache the page if the page number is outside the range
            if ($page < 1 || $page > max(ceil($total / $this->perPage), 1)) {
                throw new PageNotFoundException('Page not found: ' . Environment::get('uri'));
            }

            // Set limit and offset
            $limit  = $this->perPage;
            $offset += (max($page, 1) - 1) * $this->perPage;
            $skip   = (int) $this->skipFirst;

            // Overall limit
            if ($offset + $limit > $total + $skip) {
                $limit = $total + $skip - $offset;
            }

            // Add the pagination menu
            $objPagination = new Pagination($total, $this->perPage, Config::get('maxPaginationLinks'), $id);

            $this->Template->pagination = $objPagination->generate("\n  ");
        }

        if ($intTotal) {
            $this->Template->products = $this->parseProducts($this->fetchProductItems((int) $objPage->id, $limit, $offset), $this->attribute);
        }
    }


    /**
     * Count the product items
     *
     * @param int $intCategory The category ID
     *
     * @return int
     */
    protected function countProductItems(int $intCategory)
    {
        $criteria = $this->getSearchCriteria();

        $criteria->setCategory($intCategory);

        if ($criteria === null) {
            return 0;
        }

        return $criteria->countProducts();
    }

    /**
     * Get the search criteria
     *
     * @return ProductCriteria|null
     */
    protected function getSearchCriteria()
    {
        return System::getContainer()
            ->get('kehr_solutions.device_management.criteria.dm_builder')
            ->getCriteriaForProductList('list');
    }

    /**
     * Fetch the product items
     *
     * @param int      $intCategory The category ID
     * @param null|int $limit
     * @param null|int $offset
     *
     * @return null
     */
    protected function fetchProductItems(int $intCategory, $limit = null, $offset = null)
    {
        $criteria = $this->getSearchCriteria();
        $criteria->setCategory($intCategory);

        if ($criteria === null) {
            return null;
        }

        if ($limit) {
            $criteria->setLimit($limit);
            $criteria->setOffset($offset);
        }

        $this->attribute = $criteria->getAttribute();

        $objCategory = PageModel::findWithDetails($intCategory);

        if ($objCategory !== null && $objCategory->isProductPage && $objCategory->dmProductType) {
            $objProductType = DmProductTypeModel::findByPk($objCategory->dmProductType);

            if ($objProductType !== null) {
                $arrOrderBy = StringUtil::deserialize($objProductType->sorting, true);

                foreach ($arrOrderBy as $sort) {
                    $criteria->setOrderBy("p.$sort");
                }
            }
        }

        return $criteria->getProducts();
    }
}