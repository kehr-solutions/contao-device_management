<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Module;


use Contao\BackendTemplate;
use Contao\PageModel;
use Patchwork\Utf8;

class CategoryList extends DmModule
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'mod_dm_category_list';

    /**
     * Display a wildcard in the back end
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            /** @var BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['FMD']['dmCategoryList'][0]) . ' ###';
            $objTemplate->title    = $this->headline;
            $objTemplate->id       = $this->id;
            $objTemplate->name     = $this->name;
            $objTemplate->href     = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        if (!$this->dmRootPage) {
            return '';
        }

        return parent::generate();
    }

    // toDo - Auflistung der Kategorien neu gestalten

    protected function compile()
    {
        $objRootCategory = PageModel::findPublishedByPid($this->dmRootPage, ['order' => 'sorting']);

        $this->Template->content = $this->parseCategories($objRootCategory);
    }
}