<?php

declare(strict_types=1);

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Listener;


use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\PageModel;
use Contao\StringUtil;
use Doctrine\DBAL\Connection;
use KehrSolutions\DeviceManagementBundle\Model\DmProductModel;
use KehrSolutions\DeviceManagementBundle\Module\DmModule;

class InsertTagsListener
{
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var array
     */
    private static $supportedTags = [
        'product',
        'product_url',
        'product_title'
    ];

    public function __construct(ContaoFrameworkInterface $framework, Connection $db)
    {
        $this->framework = $framework;
        $this->db        = $db;
    }

    /**
     * Replaces device management insert tags.
     *
     * @param string $tag
     * @param bool   $useCase
     * @param null   $cacheValue
     * @param array  $flags
     *
     * @return string|false
     * @throws \Exception
     */
    public function onReplaceInsertTags(string $tag, bool $useCase = true, $cacheValue = null, array $flags = [])
    {
        $elements = explode('::', $tag);
        $key      = strtolower($elements[0]);

        if (in_array($key, self::$supportedTags, true)) {
            return $this->replaceDeviceManagementInsertTags($key, $elements[1], $flags);
        }

        return false;
    }

    /**
     * Replace a device management-related insert tag
     *
     * @param string $insertTag
     * @param string $idOrAlias
     * @param array  $flags
     *
     * @return string
     * @throws \Exception
     */
    private function replaceDeviceManagementInsertTags(string $insertTag, string $idOrAlias, array $flags): string
    {
        $this->framework->initialize();

        /** @var DmProductModel $adapter */
        $adapter = $this->framework->getAdapter(DmProductModel::class);

        if (null === ($product = $adapter->findPublishedByIdOrAlias($idOrAlias))) {
            return '';
        }

        return $this->generateProductReplacement($product, $insertTag, $flags);
    }

    /**
     *
     * Generate the replacement string.
     *
     * @param DmProductModel $product
     * @param string         $insertTag
     * @param array          $flags
     *
     * @return string
     * @throws \Exception
     */
    private function generateProductReplacement(DmProductModel $product, string $insertTag, array $flags): string
    {
        /** @var DmModule $adapter */
        $adapter = $this->framework->getAdapter(DmModule::class);

        switch ($insertTag) {
            case 'product':
                $strPageAlias = '';
                $category     = $this->db->fetchColumn("SELECT page_id FROM tl_dm_product_category WHERE product_id=?", [$product->id]);

                if (!$category) {
                    return $strPageAlias;
                }

                $objCategory = PageModel::findPublishedById($category);

                if ($objCategory->dmSetReaderJumpTo) {
                    $strPageAlias = $objCategory->alias;

                    if (($objReaderPage = $objCategory->getRelated('dmReaderJumpTo')) !== null) {
                        $strPageAlias = $objReaderPage->alias;
                    }
                }

                return sprintf(
                    '<a href="%s" title="%s">%s</a>',
                    $adapter->generateUrl(
                        $strPageAlias . '/{product}',
                        [
                            'product'   => $product->alias,
                            'auto_item' => 'product',
                            'c'         => $objCategory->id
                        ]
                    ),
                    StringUtil::specialchars($product->pageTitle ?: $product->name),
                    $product->name
                );
            case 'product_url':
                $strPageAlias = '';
                $category     = $this->db->fetchColumn("SELECT page_id FROM tl_dm_product_category WHERE product_id=?", [$product->id]);

                if (!$category) {
                    return $strPageAlias;
                }

                $objCategory = PageModel::findPublishedById($category);

                if ($objCategory->dmSetReaderJumpTo) {
                    $strPageAlias = $objCategory->alias;

                    if (($objReaderPage = $objCategory->getRelated('dmReaderJumpTo')) !== null) {
                        $strPageAlias = $objReaderPage->alias;
                    }
                }

                return $adapter->generateUrl(
                    $strPageAlias . '/{product}',
                    [
                        'product'   => $product->alias,
                        'auto_item' => 'product',
                        'c'         => $objCategory->id
                    ]
                );
            case 'product_title':
                return StringUtil::specialchars($product->pageTitle ?: $product->name);
        }
    }
}