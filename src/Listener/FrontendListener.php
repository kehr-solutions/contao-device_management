<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\Listener;


use Contao\Config;
use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\Database;
use Contao\Date;
use Contao\PageModel;
use Contao\System;
use Doctrine\DBAL\Connection;
use Haste\Input\Input;
use KehrSolutions\DeviceManagementBundle\Criteria\ProductCriteria;
use KehrSolutions\DeviceManagementBundle\Model\DmProductModel;

class FrontendListener implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * @var Connection
     */
    private $db;

    /**
     * DmProducer constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Show product name in breadcrumb
     *
     * @param array $items
     *
     * @return array
     */
    public function addProductToBreadcrumb(array $items)
    {
        /** @var PageModel|object $objPage */
        global $objPage;

        if (!($alias = Input::getAutoItem('product', false, true))
            || ($objProduct = DmProductModel::findPublishedByIdOrAlias($alias)) === null) {
            return $items;
        }

        $last = count($items) - 1;

        // If we have a reader page, rename the last item (the reader) to the product title
        if ($objPage->parentAlias && ($objParentPage = PageModel::findPublishedByIdOrAlias($objPage->parentAlias)) !== null && $objParentPage->dmSetReaderJumpTo) {
            $items[$last]['link'] = $objProduct->name;
        }

        return $items;
    }

    /**
     * Add products to the indexer
     *
     * @param array $arrPages
     * @param int   $intRoot
     * @param bool  $blnIsSitemap
     *
     * @return array
     * @throws \Exception
     */
    public function getSearchablePages($arrPages, $intRoot = 0, $blnIsSitemap = false)
    {
        $arrRoot = [];

        if ($intRoot > 0) {
            $arrRoot = Database::getInstance()->getChildRecords($intRoot, 'tl_page');
        }

        $time = Date::floorToMinute();

        // Get all product categories
        $categories = $this->db->fetchAll("SELECT DISTINCT(page_id) FROM tl_dm_product_category");

        if (is_array($categories) && !empty($categories)) {
            foreach ($categories as $key => $category) {
                // Skip category outside the root nodes
                if (!empty($arrRoot) && !in_array($category['page_id'], $arrRoot)) {
                    continue;
                }

                $objParent = PageModel::findWithDetails($category['page_id']);

                // The target page does not exist
                if ($objParent === null) {
                    continue;
                }

                // The target page has not been published
                if (!$objParent->published || ($objParent->start != '' && $objParent->start > $time) || ($objParent->stop != '' && $objParent->stop <= ($time + 60))) {
                    continue;
                }

                if ($blnIsSitemap) {
                    // The target page is protected
                    if ($objParent->protected) {
                        continue;
                    }

                    // The target page is exempt from the sitemap
                    if ($objParent->sitemap == 'map_ever') {
                        continue;
                    }
                }

                $criteria = $this->getSearchCriteria();
                $criteria->setCategory((int) $objParent->id);

                $arrProducts = $criteria->getProducts();

                if (!empty($arrProducts) && $objParent->dmSetReaderJumpTo) {
                    $strPageAlias = $objParent->getAbsoluteUrl(Config::get('useAutoItem') ? '/%s' : '/product/%s');

                    if (($objReaderPage = $objParent->getRelated('dmReaderJumpTo')) !== null) {
                        $strPageAlias = $objReaderPage->getAbsoluteUrl(Config::get('useAutoItem') ? '/%s' : '/product/%s');
                    }

                    foreach ($arrProducts as $product) {
                        // The product has not been published
                        if ($product['published'] || ($product['start'] != '' && $product['start'] > $time) || ($product['stop'] != '' && $product['stop'] <= ($time + 60))) {
                            continue;
                        }

                        $arrPages[] = sprintf(
                                          preg_replace('/%(?!s)/', '%%', $strPageAlias),
                                          ($product['alias'] ?: $product['id'])
                                      ) . '?c=' . $objParent->id;
                    }
                }
            }
        }

        return $arrPages;
    }

    /**
     * Get the search criteria
     *
     * @return ProductCriteria|null
     */
    protected function getSearchCriteria()
    {
        return System::getContainer()
            ->get('kehr_solutions.device_management.criteria.dm_builder')
            ->getCriteriaForProductList();
    }
}