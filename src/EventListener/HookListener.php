<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\EventListener;


use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\Environment;
use Contao\PageModel;

class HookListener implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    public function loadReaderPageFromUrl($arrFragments)
    {
        $strKey   = 'product';
        $strAlias = '';

        // Find products alias. Can't use Input because they're not yet initialized
        if ($GLOBALS['TL_CONFIG']['useAutoItem'] && in_array($strKey, $GLOBALS['TL_AUTO_ITEM'])) {
            $strKey = 'auto_item';
        }

        for ($i = 1, $c = count($arrFragments); $i < $c; $i += 2) {
            if ($arrFragments[$i] == $strKey) {
                $strAlias = $arrFragments[$i + 1];
            }
        }

        global $objDmListPage;
        $objDmListPage = null;

        if ($strAlias != '' && ($objPage = PageModel::findPublishedByIdOrAlias($arrFragments[0])) !== null) {
            // Check the URL and language of each page if there are multiple results
            // see Contao's index.php
            if ($objPage !== null && $objPage->count() > 1) {
                $objNewPage = null;
                $arrPages   = [];

                // Order by domain and language
                /** @var PageModel $objCurrentPage */
                foreach ($objPage as $objCurrentPage) {
                    $objCurrentPage->loadDetails();

                    $domain                                           = $objCurrentPage->domain ?: '*';
                    $arrPages[$domain][$objCurrentPage->rootLanguage] = $objCurrentPage;

                    // Also store the fallback language
                    if ($objCurrentPage->rootIsFallback) {
                        $arrPages[$domain]['*'] = $objCurrentPage;
                    }
                }

                $strHost = Environment::get('host');

                // Look for a root page whose domain name matches the host name
                if (isset($arrPages[$strHost])) {
                    $arrLangs = $arrPages[$strHost];
                } else {
                    $arrLangs = $arrPages['*']; // Empty domain
                }

                // Use the first result (see #4872)
                if (!$GLOBALS['TL_CONFIG']['addLanguageToUrl']) {
                    $objNewPage = current($arrLangs);
                } // Try to find a page matching the language parameter
                elseif (($lang = \Input::get('language')) != '' && isset($arrLangs[$lang])) {
                    $objNewPage = $arrLangs[$lang];
                }

                // Store the page object
                if (is_object($objNewPage)) {
                    $objPage = $objNewPage;
                }
            }

            if ($objPage->dm_setReaderJumpTo && ($objReader = $objPage->getRelated('dm_readerJumpTo')) !== null) {
                /** @var \PageModel $objDmListPage */
                $objDmListPage = $objPage->current();
                $objDmListPage->loadDetails();

                $arrFragments[0] = ($objReader->alias ?: $objReader->id);
            }
        }

        return $arrFragments;
    }
}