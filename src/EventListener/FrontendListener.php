<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

namespace KehrSolutions\DeviceManagementBundle\EventListener;


use Contao\CoreBundle\Framework\FrameworkAwareInterface;
use Contao\CoreBundle\Framework\FrameworkAwareTrait;
use Contao\PageModel;

class FrontendListener implements FrameworkAwareInterface
{
    use FrameworkAwareTrait;

    /**
     * Show product name in breadcrumb
     *
     * @param array $arrItems
     *
     * @return array
     */
    public function addProductToBreadcrumb($arrItems)
    {
        /** @var PageModel|object $objPage */
        global $objPage;

        global $objDmListPage;

        $last = count($arrItems) - 1;

        /*if (null !== $objDmListPage) {

        } else {
            $arrItems[$last]['href']     = 'etwas';
            $arrItems[$last]['isActive'] = false;

            $arrItems[] = [
                'isRoot'   => false,
                'isActive' => true,
                'href'     => 'product',
                'title'    => 'Product Name',
                'link'     => 'Product Name Link',
                'data'     => []
            ];
        }*/

        return $arrItems;
    }
}