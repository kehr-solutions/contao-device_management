<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['device_management'] = 'Geräteverwaltung';
$GLOBALS['TL_LANG']['tl_nc_notification']['type']['request_form']      = ['Anfrageformular', 'Dieser Benachrichtigungstyp wird versendet, wenn ein Benutzer eine Anfrage zum Produkt hat.'];