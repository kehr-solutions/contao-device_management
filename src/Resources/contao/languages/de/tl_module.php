<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['dmDefaultImage']        = ['Bild', 'Das Standard-Bild wird herangezogen wenn es kein Bild für die Kategorie oder das Produkt definiert wurde.'];
$GLOBALS['TL_LANG']['tl_module']['dm_template']           = ['Geräteverwaltung-Template', 'Bitte wählen Sie das Template aus.'];
$GLOBALS['TL_LANG']['tl_module']['dmSetJumpTo']           = ['Eine Weiterleitungsseite festlegen', 'Dem Modul eine individuelle Zielseite zuweisen. (z.B. für Hersteller)'];
$GLOBALS['TL_LANG']['tl_module']['dmJumpTo']              = ['Referenzseite', 'Bitte wählen Sie die Referenzseite aus der Seitenstruktur.'];
$GLOBALS['TL_LANG']['tl_module']['showSliderControls']    = ['Slider-Controls', 'Wählen Sie diese Option aus, wenn die Slider-Controls im Fontend angezeigt werden sollen.'];
$GLOBALS['TL_LANG']['tl_module']['showThumb']             = ['Slider-Thumbnails', 'Wählen Sie diese Option aus, wenn die Slider-Thumbnails im Fontend angeziegt werden sollen.'];
$GLOBALS['TL_LANG']['tl_module']['sliderInterval']        = ['Slider-Interval', 'Hinterlegen Sie hier den Interval für den Slider. Geben Sie 0 ein, um den automatischen Slider zu deaktivieren.'];
$GLOBALS['TL_LANG']['tl_module']['dmRootPage']            = ['Referenzseite', 'Bitte wählen Sie die Referenzseite aus der Seitenstuktur.'];
$GLOBALS['TL_LANG']['tl_module']['dmRequestForm']         = ['Anfrage-Formular', 'Bitte wählen Sie ein Anfrage-Formular aus.'];
$GLOBALS['TL_LANG']['tl_module']['dmSetNew']              = ['Hinweis NEU anzeigen', 'Wählen Sie die Option aus, wenn Produkte den Hinweis NEU angeziegt bekommen sollen.'];
$GLOBALS['TL_LANG']['tl_module']['dmNewDays']             = ['Anzeige-Dauer', 'Hinterlegen Sie die Anzahl der Tage, wie lange der Hinweis erscheinen soll.'];
$GLOBALS['TL_LANG']['tl_module']['dmShowProducerWebsite'] = ['Hersteller Homepage', 'Wählen Sie diese Option aus, wenn auf der Webseite der Link zur Hersteller-Homepage angezeigt werden soll.'];