<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */
$GLOBALS['TL_LANG']['MSC']['dataSheets']       = 'Datenblätter';
$GLOBALS['TL_LANG']['MSC']['equipment']        = 'Ausstattung';
$GLOBALS['TL_LANG']['MSC']['requestForm']      = 'Anfrageformular';
$GLOBALS['TL_LANG']['MSC']['dmReadMore']       = 'Mehr erfahren';
$GLOBALS['TL_LANG']['MSC']['emptyProductList'] = 'Aktuell befinden sich in dieser Kategorie keine Produkte.';
$GLOBALS['TL_LANG']['MSC']['moreProducts']     = 'Weitere Produkte von %s';
$GLOBALS['TL_LANG']['MSC']['readMoreProducts'] = 'Weitere Informationen zum Produkt: %s';
$GLOBALS['TL_LANG']['MSC']['productPicker']    = 'Produkte';
$GLOBALS['TL_LANG']['MSC']['producerPicker']   = 'Hersteller';
$GLOBALS['TL_LANG']['MSC']['noPrice']          = 'auf Anfrage';
$GLOBALS['TL_LANG']['MSC']['labelNew']         = 'Neu';
$GLOBALS['TL_LANG']['MSC']['productsFrom']     = 'Produkte von ';
$GLOBALS['TL_LANG']['MSC']['goToHomepage']     = 'Zur Homepage von ';

/**
 * Tooltips
 */
$GLOBALS['TL_LANG']['MSC']['tooltips'] = [
    'indoor'       => 'Im Innenbereich verwendbar',
    'outdoor'      => 'Im Außenbereich verwendbar',
    'width'        => 'Breite: ',
    'height'       => 'Arbeitshöhe: ',
    'loadCapacity' => 'Tragfähigkeit: '
];