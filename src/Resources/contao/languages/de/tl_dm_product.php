<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_dm_product']['new']    = ['Neues Produkt', 'Ein neues Produkt anlegen'];
$GLOBALS['TL_LANG']['tl_dm_product']['edit']   = ['Produkt bearbeiten', 'Produkt ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_dm_product']['copy']   = ['Produkt duplizieren', 'Produkt ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_dm_product']['delete'] = ['Produkt löschen', 'Produkt ID %s löschen'];
$GLOBALS['TL_LANG']['tl_dm_product']['toggle'] = ['Sichtbarkeit ändern', 'Die Sichtbarkeit des Produktes ID %s ändern'];
$GLOBALS['TL_LANG']['tl_dm_product']['show']   = ['Produktdetails', 'Details des Produktes ID %s anzeigen'];

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_dm_product']['title_legend']     = 'Name & Alias';
$GLOBALS['TL_LANG']['tl_dm_product']['producer_legend']  = 'Hersteller-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_product']['category_legend']  = 'Kategorie-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_product']['text_legend']      = 'Produktbeschreibung';
$GLOBALS['TL_LANG']['tl_dm_product']['image_legend']     = 'Bild-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_product']['working_legend']   = 'Arbeitswerte';
$GLOBALS['TL_LANG']['tl_dm_product']['speed_legend']     = 'Geschwindigkeit';
$GLOBALS['TL_LANG']['tl_dm_product']['transport_legend'] = 'Transport';
$GLOBALS['TL_LANG']['tl_dm_product']['other_legend']     = 'Sonstiges';
$GLOBALS['TL_LANG']['tl_dm_product']['sheet_legend']     = 'Downloads';
$GLOBALS['TL_LANG']['tl_dm_product']['general_legend']   = 'Allgemeine-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_product']['equipment_legend'] = 'Ausstattung';
$GLOBALS['TL_LANG']['tl_dm_product']['form_legend']      = 'Formular';
$GLOBALS['TL_LANG']['tl_dm_product']['meta_legend']      = 'Metadaten';
$GLOBALS['TL_LANG']['tl_dm_product']['publish_legend']   = 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_dm_product']['name']              = ['Produktname', 'Bitte geben Sie den Namen des Produktes ein.'];
$GLOBALS['TL_LANG']['tl_dm_product']['alias']             = ['Produktalias', 'Der Produktalias ist eine eindeutige Referenz, die anstelle der numerischen Produkt-ID aufgerufen werden kann.'];
$GLOBALS['TL_LANG']['tl_dm_product']['producerId']        = ['Hersteller', 'Bitte wählen Sie einen Hersteller aus der Liste aus.'];
$GLOBALS['TL_LANG']['tl_dm_product']['linkProducer']      = ['Verlinkung zum Hersteller', 'Definieren Sie hier, ob eine Verlinkung zum Hersteller auf der Webseite erfolgen soll.'];
$GLOBALS['TL_LANG']['tl_dm_product']['pages']             = ['Kategorien', 'Bitte wählen Sie die Kategorien aus der Seitenstruktur aus.'];
$GLOBALS['TL_LANG']['tl_dm_product']['teaser']            = ['Teaser', 'Bitte geben Sie einen Produkt-Teaser ein.'];
$GLOBALS['TL_LANG']['tl_dm_product']['text']              = ['Beschreibung', 'Bitte geben Sie eine Produkt-Beschreibung ein.'];
$GLOBALS['TL_LANG']['tl_dm_product']['images']            = ['Quelldateien', 'Bitte wählen Sie die Dateien aus der Dateiübersicht.'];
$GLOBALS['TL_LANG']['tl_dm_product']['size']              = ['Bildgröße', 'HIer können Sie die Abmessungen des Bildes und den Skalierungsmodus festlegen'];
$GLOBALS['TL_LANG']['tl_dm_product']['sheets']            = ['Downlads', 'Bitte wählen Sie die Datenblätter aus der Dateiübersicht aus.'];
$GLOBALS['TL_LANG']['tl_dm_product']['length']            = ['Länge', 'Hinterlegen Sie hier die Länge. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['width']             = ['Breite', 'Hinterlegen Sie hier die Breite. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['height']            = ['Höhe', 'Hinterlegen Sie hier die Höhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['weight']            = ['Gewicht', 'Hinterlegen Sie hier das Gewicht. <em>(Einheit: kg)</em>', 'kg'];
$GLOBALS['TL_LANG']['tl_dm_product']['groundClearance']   = ['Bodenfreiheit', 'Hinterlegen Sie hier die Bodenfreiheit. <em>(Einheit: cm)</em>', 'cm'];
$GLOBALS['TL_LANG']['tl_dm_product']['driveSpeed']        = ['Fahrgeschwindigkeit', 'Hinterlegen Sie hier die Fahrgeschwindigkeit. <em>(Einheit: km/h)</em>', 'km/h'];
$GLOBALS['TL_LANG']['tl_dm_product']['powerSource']       = ['Antriebsart', 'Wählen Sie die Antriebsart aus der Liste aus.'];
$GLOBALS['TL_LANG']['tl_dm_product']['workingHeight']     = ['Arbeitshöhe', 'Hinterlegen Sie hier die Arbeitshöhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['platformHeight']    = ['Plattformhöhe', 'Hinterlegen Sie hier die Plattformhöhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['platformLength']    = ['Plattformlänge', 'Hinterlegen Sie hier die Plattformlänge. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['platformWidth']     = ['Plattformbreite', 'Hinterlegen Sie hier die Plattformbreite. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['liftingHeight']     = ['Hubhöhe', 'Hinterlegen Sie hier die Hubhöhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['loadCapacity']      = ['Tragfähigkeit', 'Hinterlegen Sie hier die Tragfähigkeit. <em>(Einheit: kg)</em>', 'kg'];
$GLOBALS['TL_LANG']['tl_dm_product']['outreach']          = ['seitl. Reichweite', 'Hinterlegen Sie hier die seitl. Reichweite. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['articulationPoint'] = ['Gelenkpunkt', 'Hinterlegen Sie hier den Gelenkpunkt. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['tailSwing']         = ['Überhang', 'Hinterlegen Sie hier den Überhang. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['transportWidth']    = ['Transportbreite', 'Hinterlegen Sie hier die Transportbreite. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['transportHeight']   = ['Transporthöhe', 'Hinterlegen Sie hier die Transporthöhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['transportLength']   = ['Transportlänge', 'Hinterlegen Sie hier die Transportlänge. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['indoor']            = ['Verwendbar im Innenbereich', 'Wählen Sie hier aus, wenn das Produkt im Innenbereich verwendbar ist.'];
$GLOBALS['TL_LANG']['tl_dm_product']['outdoor']           = ['Verwendbar im Außenbereich', 'Wählen Sie hier aus, wenn das Produkt im Außenbereich verwendbar ist.'];
$GLOBALS['TL_LANG']['tl_dm_product']['flyJib']            = ['Korbarmbewegung', 'Hinterlegen Sie hier die Korbarmbewegung.'];
$GLOBALS['TL_LANG']['tl_dm_product']['stowedLength']      = ['Länge in eingefahrenem Zustand', 'Hinterlegen Sie hier die Länge im eingefahrenem Zustand. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['overallWidth']      = ['Breite', 'Hinterlegen Sie hier die Breite. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['stowedHeight']      = ['Höhe (<em>eingefahren</em>)', 'Hinterlegen Sie hier die eingefahrene Höhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['gradeability']      = ['Steigfähigkeit', 'Hinterlegen Sie hier die Steigfähigkeit. <em>(Einheit: %)</em>', '%'];
$GLOBALS['TL_LANG']['tl_dm_product']['highSpeed']         = ['Höchstgeschwindigkeit', 'Hinterlegen Sie hier die Höchstgeschwindigkeit. <em>(Einheit: km/h)</em>', 'km/h'];
$GLOBALS['TL_LANG']['tl_dm_product']['outerTropic']       = ['äußerer Wendekreis', 'Hinterlegen Sie hier den äußerenen Wendekreis. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['innerTropic']       = ['innerer Wendekreis', 'Hinterlegen Sie hier den inneren Wendekreis.'];
$GLOBALS['TL_LANG']['tl_dm_product']['hookOutreach']      = ['Reichweite des Hakens', 'Hinterlegen Sie hier die Reichtweite des Hakens.'];
$GLOBALS['TL_LANG']['tl_dm_product']['rotation']          = ['Schwenkbereich', 'Hinterlegen Sie hier den Schwenkbereich.'];
$GLOBALS['TL_LANG']['tl_dm_product']['stowedHeightRail']  = ['Höhe (<em>eingefahren, mit Geländer</em>)', 'Hinterlegen Sie hier die eingefahrene Höhe mit Geländer. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['maxLiftingHeight']  = ['max. Hebehöhe', 'Hinterlegen Sie hier die max. Hebehöhe. <em>(Einheit: mm)</em>', 'mm'];
$GLOBALS['TL_LANG']['tl_dm_product']['mastHeight']        = ['Masthöhe', 'Hinterlegen Sie hier die Masthöhe. <em>(Einheit: mm)</em>', 'mm'];
$GLOBALS['TL_LANG']['tl_dm_product']['headRoom']          = ['Bauhöhe', 'Hinterlegen Sie hier die Bauhöhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['tiltedLength']      = ['Länge gekippt', 'Hinterlegen Sie hier die gekippte Länge. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['tiltedHeight']      = ['Höhe gekippt', 'Hinterlegen Sie hier die gekippte Höhe. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['payload']           = ['Nutzlast', 'Hinterlegen Sie hier die Nutzlast. <em>(Einheit: kg)</em>', 'kg'];
$GLOBALS['TL_LANG']['tl_dm_product']['liftingForce']      = ['Hubkraft', 'Hinterlegen Sie hier die Hubkraft. <em>(Einheit: kg)</em>', 'kg'];
$GLOBALS['TL_LANG']['tl_dm_product']['amplitude']         = ['Ausschub', 'Hinterlegen Sie hier den Ausschub. <em>(Einheit: m)</em>', 'm'];
$GLOBALS['TL_LANG']['tl_dm_product']['sale']              = ['Sale', 'Wählen Sie die Option aus, wenn das Produkt zum verkauf steht.'];
$GLOBALS['TL_LANG']['tl_dm_product']['manufactured']      = ['Baujahr', 'Bitte hinterlegen Sie hier das Baujahr vom Produkt.'];
$GLOBALS['TL_LANG']['tl_dm_product']['operatingHours']    = ['Betriebsstunden', 'Bitte hinterlegen Sie hier die Betriebsstunden.'];
$GLOBALS['TL_LANG']['tl_dm_product']['addPrice']          = ['Preis hinzufügen', 'Wählen Sie die Option aus, wenn Sie dem Produkt ein Verkaufs-Preis hinterlegen möchten.'];
$GLOBALS['TL_LANG']['tl_dm_product']['price']             = ['Preis', 'Bitte hinterlegen Sie hier den Verkaufspreis.', '€'];
$GLOBALS['TL_LANG']['tl_dm_product']['rent']              = ['Rent', 'Wählen Sie die Option aus, wenn das Produkt zur Vermietung ist.'];
$GLOBALS['TL_LANG']['tl_dm_product']['equipment']         = ['Ausstattung', 'Hier haben Sie die Möglichkeit die Ausstattung zu hinterlegen.'];
$GLOBALS['TL_LANG']['tl_dm_product']['showForm']          = ['Anfrageformular anzeigen', 'Definieren Sie hier, ob das Anfrageformular für dieses Produkt auf der Webseite angezeigt werden soll.'];
$GLOBALS['TL_LANG']['tl_dm_product']['pageTitle']         = ['Produkttitel', 'Bitte geben Sie den Titel des Produktes ein.'];
$GLOBALS['TL_LANG']['tl_dm_product']['description']       = ['Beschreibung des Produktes', 'Hier können Sie eine kurze Beschreibung des Produktes eingeben, die von Suchmaschinen wie Google oder Yahoo ausgewertet wird. Suchmaschinen indizieren normalerweise zwischen 150 und 300 Zeichen.'];
$GLOBALS['TL_LANG']['tl_dm_product']['published']         = ['Produkt Veröffentlichen', 'Das Produkt auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_dm_product']['start']             = ['Anzeigen ab', 'Das Produkt erst ab diesem Tag auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_dm_product']['stop']              = ['Anzeigen bis', 'Das Produkt nur bis zu diesem Tag auf der Webseite anzeigen.'];

/**
 * Options
 */
$GLOBALS['TL_LANG']['tl_dm_product']['electric'] = 'Elektro';
$GLOBALS['TL_LANG']['tl_dm_product']['diesel']   = 'Diesel';
$GLOBALS['TL_LANG']['tl_dm_product']['hybrid']   = 'Hybrid';
$GLOBALS['TL_LANG']['tl_dm_product']['2x4']      = '2x4';
$GLOBALS['TL_LANG']['tl_dm_product']['4x4']      = '4x4';
$GLOBALS['TL_LANG']['tl_dm_product']['230v']      = '230V';
$GLOBALS['TL_LANG']['tl_dm_product']['400v']      = '400V';
$GLOBALS['TL_LANG']['tl_dm_product']['benzin']    = 'Benzin';
$GLOBALS['TL_LANG']['tl_dm_product']['bi-energy'] = 'BI-Energy';