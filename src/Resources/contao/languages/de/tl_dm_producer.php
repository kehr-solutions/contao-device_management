<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_dm_producer']['new']    = ['Neuen Hersteller', 'Einen neuen Hersteller erstellen'];
$GLOBALS['TL_LANG']['tl_dm_producer']['edit']   = ['Hersteller bearbeiten', 'Hersteller ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_dm_producer']['copy']   = ['Hersteller duplizieren', 'Hersteller ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_dm_producer']['delete'] = ['Hersteller löschen', 'Hertsller ID %s löschen'];
$GLOBALS['TL_LANG']['tl_dm_producer']['toggle'] = ['Sichtbarkeit ändern', 'Die Sichtbarkeit des Herstellers ID %s ändern'];
$GLOBALS['TL_LANG']['tl_dm_producer']['show']   = ['Herstellerdetails', 'Details des Herstellers ID %s anzeigen'];

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_dm_producer']['title_legend']    = 'Titel';
$GLOBALS['TL_LANG']['tl_dm_producer']['text_legend']     = 'Beschreibung';
$GLOBALS['TL_LANG']['tl_dm_producer']['logo_legend']     = 'Logo-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_producer']['url_legend']      = 'Hyperlink-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_producer']['products_legend'] = 'Produkt-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_producer']['meta_legend']     = 'Metadaten';
$GLOBALS['TL_LANG']['tl_dm_producer']['publish_legend']  = 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_dm_producer']['title']        = ['Herstellername', 'Bitte geben Sie den Namen des Herstellers ein.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['alias']        = ['Herstelleralias', 'Der Herstelleralias ist eine eindeutige Referenz, die anstelle der numerischen Hersteller-ID aufgerufen werden kann.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['text']         = ['Beschreibung', 'Hinterlegen Sie eine Beschreibung zum Hersteller.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['singleSRC']    = ['Logo', 'Wählen Sie ein Logo aus der Dateiverwaltung aus.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['showProducts'] = ['Produkte anzeigen', 'Die Produkte unterhalb der Beschreibung anzeigen.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['pageTitle']    = ['Herstellertitel', 'Bitte geben Sie den Titel des Herstellers ein.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['description']  = ['Beschreibung des Herstellers', 'Hier können Sie eine kurze Beschreibung des Herstellers eingeben, die von Suchmaschinen wie Google oder Yahoo ausgewertet wird. Suchmaschinen indizieren normalerweise zwischen 150 und 300 Zeichen.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['published']    = ['Hersteller Veröffentlichen', 'Den Hersteller auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['start']        = ['Anzeigen ab', 'Den Hersteller erst ab diesem Tag auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_dm_producer']['stop']         = ['Anzeigen bis', 'Den Hersteller nur bis zu diesem Tag auf der Webseite anzeigen.'];