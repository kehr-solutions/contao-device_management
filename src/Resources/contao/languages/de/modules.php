<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['device_management'] = 'Geräteverwaltung';
$GLOBALS['TL_LANG']['MOD']['dm_product']        = ['Produkte', 'Produkte verwalten'];
$GLOBALS['TL_LANG']['MOD']['dm_producer']       = ['Hersteller', 'Hersteller verwalten'];
$GLOBALS['TL_LANG']['MOD']['dm_producttype']    = ['Produkttypen', 'Produkt-Typen verwalten'];

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['dm_product_list']    = ['Produktliste', 'Allgemeines Listen-Modul. Zeigt Produkte oder Werte von Attributen an. Kann mit anderen Modulen (z.B. Filter-Modul) kombiniert werden um die Funktionen zu erweitern.'];
$GLOBALS['TL_LANG']['FMD']['dm_product_reader']  = ['Produktleser', 'Mit diesem Modul zeigen Sie Produktdetails an.'];
$GLOBALS['TL_LANG']['FMD']['dm_category_list']   = ['Kategorien-Liste', 'Auflistung der Kategorien'];
$GLOBALS['TL_LANG']['FMD']['dm_producer_reader'] = ['Hersteller-Leser', 'Mit diesem Modul zeigen Sie Herstellerdetails an.'];