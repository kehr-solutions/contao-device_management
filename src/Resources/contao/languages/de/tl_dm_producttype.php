<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_dm_producttype']['new']    = ['Neuen Produkttyp', 'Ein neuen Produkttyp anlegen'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['edit']   = ['Produkttyp bearbeiten', 'Produkttyp ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['copy']   = ['Produkttyp duplizieren', 'Produkttyp ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['delete'] = ['Produkttyp löschne', 'Produkttyp ID %s löschen'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['show']   = ['Produkttype-Details', 'Details des Produkttypes ID %s anzeigen'];

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_dm_producttype']['name_legend']    = 'Name';
$GLOBALS['TL_LANG']['tl_dm_producttype']['details_legend'] = 'Details-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_producttype']['list_legend']    = 'Listen-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_producttype']['filter_legend']  = 'Filter-Einstellungen';
$GLOBALS['TL_LANG']['tl_dm_producttype']['sorting_legend'] = 'Sortier-Einstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_dm_producttype']['name']    = ['Name', 'Bitte hinterlegen Sie einen Namen.'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['details'] = ['Details-Felder', 'Bitte definieren Sie die Felder für den Produkttyp'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['list']    = ['Listen-Felder', 'Definieren Sie hier die Felder die in der Listenübersicht angezeigt werden sollen.'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['filter']  = ['Filter-Felder', 'Definieren Sie hier die Felder nach denen im Frontend gefiltert werden kann.'];
$GLOBALS['TL_LANG']['tl_dm_producttype']['sorting'] = ['Sortier-Felder', 'Definieren Sie hier die Felder, nach denen im Frontend sortiert werden soll.'];