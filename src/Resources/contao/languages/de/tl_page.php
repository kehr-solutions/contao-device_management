<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_page']['dm_legend'] = 'Geräteverwaltung';


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_page']['isProductPage']     = ['Produkt-Seite', 'Wählen Sie die Option aus, wenn es sich bei dieser Seite um eine Produktseite handelt.'];
$GLOBALS['TL_LANG']['tl_page']['dmProductType']     = ['Attribute', 'Bitte wählen Sie eine Gruppe aus der Liste aus.'];
$GLOBALS['TL_LANG']['tl_page']['dmSale']            = ['Sale', 'Sollen Produkte zum Verkauf angezeigt werden.'];
$GLOBALS['TL_LANG']['tl_page']['dmRent']            = ['Rent', 'Sollen Produkte zur Miete angezeigt werden.'];
$GLOBALS['TL_LANG']['tl_page']['dmImage']           = ['Kategorie-Bild', 'Bitte wählen Sie eine Datei aus der Dateiverwaltung aus.'];
$GLOBALS['TL_LANG']['tl_page']['dmSetReaderJumpTo'] = ['Detailseite benutzen', 'Die Konfiguration dieser Seite überschreiben wenn Produktdetails angezeigt werden.'];
$GLOBALS['TL_LANG']['tl_page']['dmReaderJumpTo']    = ['Detailseite', 'Wählen Sie eine Seite aus der Seitenstruktur aus.'];
$GLOBALS['TL_LANG']['tl_page']['dmProducerPage']    = ['Weiterleitungsseite', 'Bitte wählen Sie die Hersteller-Seite aus, zu der Besucher weitergeletet werden, wenn Sie einen Hersteller anklicken.'];