<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Table tl_dm_product
 */
$GLOBALS['TL_DCA']['tl_dm_product'] = [
    // Config
    'config'      => [
        'dataContainer'     => 'Table',
        'enableVersioning'  => true,
        'onload_callback'   => [
            ['kehrsolutions_dm.data_container.dm_product', 'onLoadCallback'],
            ['kehrsolutions_dm.data_container.dm_product', 'generateXml']
        ],
        'oncreate_callback' => [
            ['kehrsolutions_dm.data_container.dm_product', 'onCreate']
        ],
        'ondelete_callback' => [
            ['kehrsolutions_dm.data_container.dm_product', 'scheduleUpdate']
        ],
        'onsubmit_callback' => [
            ['kehrsolutions_dm.data_container.dm_product', 'scheduleUpdate']
        ],
        'sql'               => [
            'keys' => [
                'id'    => 'primary',
                'alias' => 'index',
                //'published,start,stop' => 'index'
            ]
        ]
    ],

    // List
    'list'        => [
        'sorting'           => [
            'mode'        => 2,
            'fields'      => ['name'],
            'flag'        => 1,
            'panelLayout' => 'filter;sort,search,limit'
        ],
        'label'             => [
            'fields' => ['name'],
            'format' => '%s'
        ],
        'global_operations' => [
            'all' => [
                'label'      => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'       => 'act=select',
                'class'      => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            ]
        ],
        'operations'        => [
            'edit'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_product']['edit'],
                'href'  => 'act=edit',
                'icon'  => 'edit.svg'
            ],
            'copy'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_product']['copy'],
                'href'  => 'act=copy',
                'icon'  => 'copy.svg'
            ],
            'delete' => [
                'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['delete'],
                'href'       => 'act=delete',
                'icon'       => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ],
            'toggle' => [
                'label'                => &$GLOBALS['TL_LANG']['tl_dm_product']['toggle'],
                'attributes'           => 'onclick="Backend.getScrollOffset();"',
                'haste_ajax_operation' => [
                    'field'   => 'published',
                    'options' => [
                        [
                            'value' => 0,
                            'icon'  => 'invisible.svg'
                        ],
                        [
                            'value' => 1,
                            'icon'  => 'visible.svg'
                        ]
                    ]
                ]
            ],
            'show'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_product']['show'],
                'href'  => 'act=show',
                'icon'  => 'show.svg'
            ]
        ]
    ],

    // Select
    'select'      => [
        'buttons_callback' => [
            ['kehrsolutions_dm.data_container.dm_product', 'onAddAliasButton']
        ]
    ],

    // Palettes
    'palettes'    => [
        '__selector__' => ['sale', 'addPrice'],
        'default'      => '{title_legend},name,alias;
                      {producer_legend},producerId,linkProducer;
                      {category_legend},pages;
                      {text_legend},teaser,text;
                      {image_legend},images;
                      {sheet_legend},sheets;
                      {general_legend},indoor,outdoor,rent,sale;
                      {equipment_legend:hide},equipment;
                      {form_legend},showForm;
                      {meta_legend:hide},pageTitle,description;
                      {publish_legend},published,start,stop'
    ],

    // Subpalettes
    'subpalettes' => [
        'sale'     => 'manufactured,operatingHours,addPrice',
        'addPrice' => 'price'
    ],

    // Fields
    'fields'      => [
        'id'                => [
            'sql' => [
                'type'          => 'integer',
                'unsigned'      => true,
                'autoincrement' => true
            ]
        ],
        'tstamp'            => [
            'sql' => [
                'type'     => 'integer',
                'unsigned' => true,
                'default'  => 0
            ]
        ],
        'dateAdded'         => [
            'label' => &$GLOBALS['TL_LANG']['MSC']['dateAdded'],
            'eval'  => [
                'rgxp'      => 'datim',
                'doNotCopy' => true,
            ],
            'sql'   => [
                'type'     => 'integer',
                'unsigned' => true,
                'default'  => 0
            ]
        ],

        // Title
        'name'              => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['name'],
            'exclude'    => true,
            'search'     => true,
            'sorting'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'mandatory' => true,
                'tl_class'  => 'w50'
            ],
            'attributes' => [
                'systemColumn' => true,
                'feSorting'    => true,
            ],
            'sql'        => [
                'type'    => 'string',
                'default' => ''
            ]
        ],
        'alias'             => [
            'label'         => &$GLOBALS['TL_LANG']['tl_dm_product']['alias'],
            'exclude'       => true,
            'search'        => true,
            'inputType'     => 'text',
            'eval'          => [
                'rgxp'      => 'alias',
                'doNotCopy' => true,
                'unique'    => true,
                'maxlength' => 128,
                'tl_class'  => 'w50'
            ],
            'save_callback' => [
                ['kehrsolutions_dm.data_container.dm_product', 'onGenerateAlias']
            ],
            'sql'           => [
                'type'    => 'binary',
                'length'  => 128,
                'default' => ''
            ]
        ],

        // Producer
        'producerId'        => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['producerId'],
            'exclude'    => true,
            'filter'     => true,
            'sorting'    => true,
            'inputType'  => 'select',
            'foreignKey' => 'tl_dm_producer.title',
            'eval'       => [
                'mandatory'          => true,
                'includeBlankOption' => true,
                'chosen'             => true,
                'tl_class'           => 'w50'
            ],
            'relation'   => [
                'type' => 'hasOne',
                'load' => 'lazy'
            ],
            'sql'        => [
                'type'     => 'integer',
                'unsigned' => true,
                'default'  => 0
            ]
        ],
        'linkProducer'      => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['linkProducer'],
            'default'   => true,
            'exclude'   => true,
            'filter'    => true,
            'inputType' => 'checkbox',
            'eval'      => [
                'tl_class' => 'w50 m12'
            ],
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],

        // Pages
        'pages'             => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['pages'],
            'exclude'    => true,
            'filter'     => true,
            'inputType'  => 'pageTree',
            'foreignKey' => 'tl_page.title',
            'eval'       => [
                'multiple'  => true,
                'fieldType' => 'checkbox',
                'tl_class'  => 'clr'
            ],
            'relation'   => [
                'type'            => 'haste-ManyToMany',
                'load'            => 'lazy',
                'table'           => 'tl_page',
                'referenceColumn' => 'product_id',
                'fieldColumn'     => 'page_id',
                'relationTable'   => 'tl_dm_product_category'
            ]
        ],

        // Text
        'teaser'            => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['teaser'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'textarea',
            'eval'      => [
                'style'    => 'height:60px',
                'tl_class' => 'clr'
            ],
            'sql'       => [
                'type'    => 'text',
                'notnull' => false
            ]
        ],
        'text'              => [
            'label'       => &$GLOBALS['TL_LANG']['tl_dm_product']['text'],
            'exclude'     => true,
            'search'      => true,
            'inputType'   => 'textarea',
            'eval'        => [
                'rte'        => 'tinyMCE',
                'helpwizard' => true,
            ],
            'explanation' => 'insertTags',
            'sql'         => [
                'type'    => 'text',
                'notnull' => false
            ]
        ],

        // Images
        'images'            => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['images'],
            'exclude'   => true,
            'inputType' => 'fileTree',
            'eval'      => [
                'multiple'   => true,
                'fieldType'  => 'checkbox',
                'orderField' => 'orderImages',
                'files'      => true,
                'isGallery'  => true,
                'extensions' => \Contao\Config::get('validImageTypes'),
            ],
            'sql'       => [
                'type'    => 'blob',
                'notnull' => false,
            ]
        ],
        'orderImages'       => [
            'label' => &$GLOBALS['TL_LANG']['MSC']['sortOrder'],
            'eval'  => [
                'doNotShow' => true,
            ],
            'sql'   => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ],
        'size'              => [
            'label'            => &$GLOBALS['TL_LANG']['tl_dm_product']['size'],
            'exclude'          => true,
            'inputType'        => 'imageSize',
            'reference'        => &$GLOBALS['TL_LANG']['MSC'],
            'eval'             => [
                'rgxp'               => 'natural',
                'includeBlankOption' => true,
                'nospace'            => true,
                'helpwizard'         => true,
                'tl_class'           => 'w50',
            ],
            'options_callback' => function () {
                return System::getContainer()->get('contao.image.image_sizes')->getOptionsForUser(\Contao\BackendUser::getInstance());
            },
            'sql'              => [
                'type'    => 'string',
                'length'  => 64,
                'default' => ''
            ]
        ],

        // Data Sheets
        'sheets'            => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['sheets'],
            'exclude'   => true,
            'inputType' => 'fileTree',
            'eval'      => [
                'multiple'    => true,
                'fieldType'   => 'checkbox',
                'orderField'  => 'orderSheets',
                'files'       => true,
                'isDownloads' => true,
                'extensions'  => \Contao\Config::get('allowedDownload')
            ],
            'sql'       => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ],
        'orderSheets'       => [
            'label' => &$GLOBALS['TL_LANG']['MSC']['sortOrder'],
            'eval'  => [
                'doNotShow' => true,
            ],
            'sql'   => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ],

        // Attribute
        'length'            => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['length'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'width'             => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['width'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'height'            => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['height'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'weight'            => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['weight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'groundClearance'   => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['groundClearance'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'driveSpeed'        => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['driveSpeed'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'speed_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 5,
                'scale'     => 2
            ]
        ],
        'powerSource'       => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['powerSource'],
            'exclude'    => true,
            'inputType'  => 'select',
            'options'    => ['electric', 'diesel', 'hybrid', '2x4', '4x4', '230v', '400v', 'benzin', 'bi-energy'],
            'reference'  => &$GLOBALS['TL_LANG']['tl_dm_product'],
            'eval'       => [
                'includeBlankOption' => true,
                'tl_class'           => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => false,
                'beEdit'    => true,
                'fieldType' => 'select',
            ],
            'sql'        => [
                'type'    => 'string',
                'length'  => 32,
                'default' => ''
            ]
        ],
        'workingHeight'     => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['workingHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'working_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'platformHeight'    => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['platformHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'working_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'platformLength'    => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['platformLength'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'working_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'platformWidth'     => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['platformWidth'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'working_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'liftingHeight'     => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['liftingHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'loadCapacity'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['loadCapacity'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'working_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'outreach'          => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['outreach'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'articulationPoint' => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['articulationPoint'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'tailSwing'         => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['tailSwing'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'transportWidth'    => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['transportWidth'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'transport_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'transportHeight'   => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['transportHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'transport_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'transportLength'   => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['transportLength'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'transport_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'flyJib'            => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['flyJib'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'stowedLength'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['stowedLength'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'overallWidth'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['overallWidth'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'stowedHeight'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['stowedHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => true,
                'scale'     => 2
            ]
        ],
        'gradeability'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['gradeability'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'highSpeed'         => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['highSpeed'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'speed_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 5,
                'scale'     => 2
            ]
        ],
        'outerTropic'       => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['outerTropic'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'innerTropic'       => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['innerTropic'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'hookOutreach'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['hookOutreach'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'rotation'          => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['rotation'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'stowedHeightRail'  => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['stowedHeightRail'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'maxLiftingHeight'  => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['maxLiftingHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'mastHeight'        => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['mastHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'headRoom'          => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['headRoom'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'tiltedLength'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['tiltedLength'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'tiltedHeight'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['tiltedHeight'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'payload'           => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['payload'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'liftingForce'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['liftingForce'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],
        'amplitude'         => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['amplitude'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'legend'    => 'other_legend',
                'feFilter'  => true,
                'feSorting' => true,
                'beEdit'    => true,
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2,
            ]
        ],

        // Sale
        'sale'              => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['sale'],
            'exclude'    => true,
            'filter'     => true,
            'inputType'  => 'checkbox',
            'eval'       => [
                'submitOnChange' => true,
                'tl_class'       => 'w50'
            ],
            'attributes' => [
                'systemColumn' => true,
                'feSorting'    => true,
                'feFilter'     => true,
                'fieldType'    => 'boolean',
            ],
            'sql'        => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'manufactured'      => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['manufactured'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'       => 'date',
                'datepicker' => true,
                'tl_class'   => 'w50 wizard'
            ],
            'attributes' => [
                'fieldType' => 'date',
            ],
            'sql'        => [
                'type'    => 'string',
                'length'  => 10,
                'default' => ''
            ]
        ],
        'operatingHours'    => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['operatingHours'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'rgxp'     => 'digit',
                'tl_class' => 'w50',
            ],
            'attributes' => [
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 0
            ]
        ],
        'addPrice'          => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['addPrice'],
            'exclude'   => true,
            'inputType' => 'checkbox',
            'eval'      => [
                'submitOnChange' => true,
                'tl_class'       => 'w50'
            ],
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'price'             => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['price'],
            'exclude'    => true,
            'inputType'  => 'text',
            'eval'       => [
                'mandatory' => true,
                'rgxp'      => 'digit',
                'tl_class'  => 'w50'
            ],
            'attributes' => [
                'fieldType' => 'decimal',
            ],
            'sql'        => [
                'type'      => 'decimal',
                'precision' => 10,
                'scale'     => 2
            ]
        ],
        'indoor'            => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['indoor'],
            'exclude'    => true,
            'filter'     => true,
            'inputType'  => 'checkbox',
            'eval'       => [
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'systemColumn' => true,
                'feFilter'     => true,
                'feSorting'    => true,
            ],
            'sql'        => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'outdoor'           => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['outdoor'],
            'exclude'    => true,
            'default'    => true,
            'filter'     => true,
            'inputType'  => 'checkbox',
            'eval'       => [
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'systemColumn' => true,
                'feFilter'     => true,
                'feSorting'    => true,
            ],
            'sql'        => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'rent'              => [
            'label'      => &$GLOBALS['TL_LANG']['tl_dm_product']['rent'],
            'exclude'    => true,
            'filter'     => true,
            'inputType'  => 'checkbox',
            'eval'       => [
                'tl_class' => 'w50'
            ],
            'attributes' => [
                'systemColumn' => true,
                'feFilter'     => true,
                'feSorting'    => true,
            ],
            'sql'        => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],

        // Ausstattung
        'equipment'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['equipment'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'textarea',
            'eval'      => [
                'rte' => 'tinyMCE'
            ],
            'sql'       => [
                'type'    => 'text',
                'notnull' => false
            ]
        ],

        // Form
        'showForm'          => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['showForm'],
            'default'   => true,
            'exclude'   => true,
            'inputType' => 'checkbox',
            'eval'      => [
                'tl_class' => 'w50',
            ],
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],

        // Meta
        'pageTitle'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['pageTitle'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'maxlength'      => 255,
                'decodeEntities' => true,
                'tl_class'       => 'w50',
            ],
            'sql'       => [
                'type'    => 'string',
                'default' => ''
            ]
        ],
        'description'       => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['description'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'textarea',
            'eval'      => [
                'style'          => 'height:60px',
                'decodeEntities' => true,
                'tl_class'       => 'clr',
            ],
            'sql'       => [
                'type'    => 'text',
                'notnull' => false,
            ]
        ],

        // Publish
        'published'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['published'],
            'exclude'   => true,
            'filter'    => true,
            'inputType' => 'checkbox',
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'start'             => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['start'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'rgxp'       => 'datim',
                'datepicker' => true,
                'tl_class'   => 'w50 wizard'
            ],
            'sql'       => [
                'type'    => 'string',
                'length'  => 10,
                'default' => ''
            ]
        ],
        'stop'              => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_product']['stop'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'rgxp'       => 'datim',
                'datepicker' => true,
                'tl_class'   => 'w50 wizard'
            ],
            'sql'       => [
                'type'    => 'string',
                'length'  => 10,
                'default' => ''
            ]
        ]
    ],
];