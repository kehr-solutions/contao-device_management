<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'dmSetJumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'dmSetNew';


$GLOBALS['TL_DCA']['tl_module']['palettes']['dm_category_list']   = '{title_legend},name,headline,type;{config_legend},dmDefaultImage,imgSize;{reference_legend},dmRootPage;{template_legend:hide},dm_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['dm_product_list']    = '{title_legend},name,headline,type;{config_legend},perPage,numberOfItems;{image_legend},dmDefaultImage,imgSize;{template_legend:hide},dm_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['dm_product_reader']  = '{title_legend},name,headline,type;{config_legend},showSliderControls,showThumb,sliderInterval,dmRequestForm;{reference_legend},dmSetJumpTo;{image_legend},dmDefaultImage,imgSize;{template_legend:hide},dm_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['dm_product_news']    = '{title_legend},name,headline,type;{config_legend},numberOfItems,dmSetNew,imgSize;{template_legend:hide},dm_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['dm_producer_reader'] = '{title_legend},name,headline,type;{config_legend},dmDefaultImage,imgSize,dmShowProducerWebsite;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';


/**
 * Add subpalettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['dmSetJumpTo'] = 'dmJumpTo';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['dmSetNew']    = 'dmNewDays';


/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['dm_template'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['dm_template'],
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => ['kehrsolutions_dm.data_container.contao.module', 'onGetDmTemplates'],
    'eval'             => [
        'mandatory'          => true,
        'includeBlankOption' => true,
        'chosen'             => true,
        'tl_class'           => 'w50'
    ],
    'sql'              => [
        'type'    => 'string',
        'length'  => 64,
        'default' => ''
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmRootPage'] = [
    'label'      => &$GLOBALS['TL_LANG']['tl_module']['dmRootPage'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => [
        'mandatory' => true,
        'fieldType' => 'radio',
        'tl_class'  => 'clr'
    ],
    'sql'        => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ],
    'relation'   => [
        'type' => 'hasOne',
        'load' => 'lazy'
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmDefaultImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['dmDefaultImage'],
    'exclude'   => true,
    'inputType' => 'fileTree',
    'eval'      => [
        'mandatory'  => true,
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => implode(',', \Contao\System::getContainer()->getParameter('contao.image.valid_extensions')),
        'tl_class'   => 'clr'
    ],
    'sql'       => [
        'type'    => "binary",
        'length'  => 16,
        'notnull' => false
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmSetJumpTo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['dmSetJumpTo'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'submitOnChange' => true,
        'tl_class'       => 'clr',
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmJumpTo'] = [
    'label'      => &$GLOBALS['TL_LANG']['tl_module']['dmJumpTo'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => [
        'mandatory' => true,
        'fieldType' => 'radio'
    ],
    'sql'        => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ],
    'relation'   => [
        'type' => 'hasOne',
        'load' => 'lazy'
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['sliderInterval'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['sliderInterval'],
    'default'   => 5000,
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => [
        'rgxp'     => 'natural',
        'tl_class' => 'w50 clr'
    ],
    'sql'       => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['showSliderControls'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['showSliderControls'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'tl_class' => 'w50',
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['showThumb'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['showThumb'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'tl_class' => 'w50',
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmRequestForm'] = [
    'label'      => &$GLOBALS['TL_LANG']['tl_module']['dmRequestForm'],
    'exclude'    => true,
    'inputType'  => 'select',
    'foreignKey' => 'tl_form.title',
    'eval'       => [
        'includeBlankOption' => true,
        'chosen'             => true,
        'tl_class'           => 'w50'
    ],
    'sql'        => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ],
    'relation'   => [
        'type' => 'hasOne',
        'load' => 'lazy'
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmSetNew'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['dmSetNew'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'submitOnChange' => true,
        'tl_class'       => 'w50'
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmNewDays'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['dmNewDays'],
    'exclude'   => true,
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => true,
        'rgxp'      => 'digit',
        'tl_class'  => 'w50'
    ],
    'sql'       => [
        'type'      => 'decimal',
        'precision' => 10,
        'scale'     => 0
    ]
];

$GLOBALS['TL_DCA']['tl_module']['fields']['dmShowProducerWebsite'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['dmShowProducerWebsite'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'tl_class' => 'w50 m12'
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];