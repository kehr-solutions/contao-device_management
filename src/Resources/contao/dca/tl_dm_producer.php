<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Table tl_dm_producer
 */
$GLOBALS['TL_DCA']['tl_dm_producer'] = [
    // Config
    'config'   => [
        'dataContainer'     => 'Table',
        'enableVersioning'  => true,
        'onload_callback' => [
            ['kehrsolutions_dm.data_container.dm_producer', 'onGenerateSitemap']
        ],
        'ondelete_callback' => [
            ['kehrsolutions_dm.data_container.dm_producer', 'onScheduleUpdate']
        ],
        'onsubmit_callback' => [
            ['kehrsolutions_dm.data_container.dm_producer', 'onScheduleUpdate']
        ],
        'sql'               => [
            'keys' => [
                'id'    => 'primary',
                'alias' => 'index'
            ]
        ]
    ],

    // List
    'list'     => [
        'sorting'           => [
            'mode'        => 1,
            'fields'      => ['title'],
            'flag'        => 1,
            'panelLayout' => 'filter;search,limit'
        ],
        'label'             => [
            'fields' => ['title'],
            'format' => '%s'
        ],
        'global_operations' => [
            'all' => [
                'label'      => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'       => 'act=select',
                'class'      => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            ]
        ],
        'operations'        => [
            'edit'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_producer']['edit'],
                'href'  => 'act=edit',
                'icon'  => 'edit.svg'
            ],
            'copy'   => [
                'label'      => &$GLOBALS['TL_LANG']['tl_dm_producer']['copy'],
                'href'       => 'act=copy',
                'icon'       => 'copy.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"'
            ],
            'delete' => [
                'label'           => &$GLOBALS['TL_LANG']['tl_dm_producer']['delete'],
                'href'            => 'act=delete',
                'icon'            => 'delete.svg',
                'attributes'      => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
                'button_callback' => ['kehrsolutions_dm.data_container.dm_producer', 'onDeleteProducer']
            ],
            'toggle' => [
                'label'                => &$GLOBALS['TL_LANG']['tl_dm_producer']['toggle'],
                'attributes'           => 'onclick="Backend.getScrollOffset();"',
                'haste_ajax_operation' => [
                    'field'   => 'published',
                    'options' => [
                        [
                            'value' => 0,
                            'icon'  => 'invisible.svg',
                        ],
                        [
                            'value' => 1,
                            'icon'  => 'visible.svg',
                        ]
                    ]
                ],
            ],
            'show'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_producer']['show'],
                'href'  => 'act=show',
                'icon'  => 'show.svg'
            ]
        ],
    ],

    // Select
    'select'   => [
        'buttons_callback' => [
            ['kehrsolutions_dm.data_container.dm_producer', 'onAddAliasButton']
        ]
    ],

    // Palettes
    'palettes' => [
        'default' => '{title_legend},title,alias;{text_legend},text;{logo_legend},singleSRC;{url_legend},url,target;{products_legend:hide},showProducts;{meta_legend},pageTitle,description;{publish_legend},published,start,stop'
    ],

    // Fields
    'fields'   => [
        'id'           => [
            'sql' => [
                'type'          => 'integer',
                'unsigned'      => true,
                'autoincrement' => true,
            ]
        ],
        'tstamp'       => [
            'sql' => [
                'type'     => 'integer',
                'unsigned' => true,
                'default'  => 0
            ]
        ],
        'title'        => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['title'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'mandatory'      => true,
                'decodeEntities' => true,
                'maxlength'      => 255,
                'tl_class'       => 'w50'
            ],
            'sql'       => [
                'type'    => 'string',
                'default' => ''
            ]
        ],
        'alias'        => [
            'label'         => &$GLOBALS['TL_LANG']['tl_dm_producer']['alias'],
            'exclude'       => true,
            'search'        => true,
            'inputType'     => 'text',
            'eval'          => [
                'rgxp'      => 'alias',
                'doNotCopy' => true,
                'unique'    => true,
                'maxlength' => 128,
                'tl_class'  => 'w50'
            ],
            'save_callback' => [
                ['kehrsolutions_dm.data_container.dm_producer', 'onGenerateAlias'],
            ],
            'sql'           => [
                'type'    => 'binary',
                'length'  => 128,
                'default' => ''
            ]
        ],
        'text'         => [
            'label'       => &$GLOBALS['TL_LANG']['tl_dm_producer']['text'],
            'exclude'     => true,
            'search'      => true,
            'inputType'   => 'textarea',
            'eval'        => [
                'rte'        => 'tinyMCE',
                'helpwizard' => true,
            ],
            'explanation' => 'insertTags',
            'sql'         => [
                'type'       => 'text',
                'MEDIUMTEXT' => true,
                'notnull'    => false
            ]
        ],
        'singleSRC'    => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['singleSRC'],
            'exclude'   => true,
            'inputType' => 'fileTree',
            'eval'      => [
                'filesOnly'  => true,
                'fieldType'  => 'radio',
                'extensions' => implode(',', \Contao\System::getContainer()->getParameter('contao.image.valid_extensions')),
                'tl_class'   => 'clr'
            ],
            'sql'       => [
                'type'    => "binary",
                'length'  => 16,
                'notnull' => false
            ]
        ],
        'url'          => [
            'label'     => &$GLOBALS['TL_LANG']['MSC']['url'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'rgxp'           => 'url',
                'decodeEntities' => true,
                'maxlength'      => 255,
                'dcaPicker'      => true,
                'tl_class'       => 'w50 wizard',
            ],
            'sql'       => [
                'type'    => 'string',
                'default' => ''
            ]
        ],
        'target'       => [
            'label'     => &$GLOBALS['TL_LANG']['MSC']['target'],
            'exclude'   => true,
            'filter'    => true,
            'inputType' => 'checkbox',
            'eval'      => [
                'tl_class' => 'w50 m12',
            ],
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'showProducts' => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['showProducts'],
            'exclude'   => true,
            'filter'    => true,
            'inputType' => 'checkbox',
            'eval'      => [
                'tl_class' => 'w50',
            ],
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'pageTitle'    => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['pageTitle'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'maxlength'      => 255,
                'decodeEntities' => true,
                'tl_class'       => 'w50'
            ],
            'sql'       => [
                'type'    => 'string',
                'default' => ''
            ]
        ],
        'description'  => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['description'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'textarea',
            'eval'      => [
                'style'          => 'height:60px',
                'decodeEntities' => true,
                'tl_class'       => 'clr'
            ],
            'sql'       => [
                'type'    => 'text',
                'notnull' => false
            ]
        ],
        'published'    => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['published'],
            'exclude'   => true,
            'filter'    => true,
            'inputType' => 'checkbox',
            'eval'      => [
                'doNotCopy' => true,
            ],
            'sql'       => [
                'type'    => 'boolean',
                'default' => 0
            ]
        ],
        'start'        => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['start'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'rgxp'       => 'datim',
                'datepicker' => true,
                'tl_class'   => 'w50 wizard',
            ],
            'sql'       => [
                'type'    => 'string',
                'length'  => 10,
                'default' => ''
            ]
        ],
        'stop'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producer']['stop'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'rgxp'       => 'datim',
                'datepicker' => true,
                'tl_class'   => 'w50 wizard',
            ],
            'sql'       => [
                'type'    => 'string',
                'length'  => 10,
                'default' => ''
            ]
        ]
    ]
];