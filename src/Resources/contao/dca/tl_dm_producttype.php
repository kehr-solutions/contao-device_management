<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

\Contao\Controller::loadLanguageFile('tl_dm_product');
/**
 * Table tl_dm_producttype
 */
$GLOBALS['TL_DCA']['tl_dm_producttype'] = [
    // Config
    'config'   => [
        'dataContainer' => 'Table',
        'sql'           => [
            'keys' => [
                'id' => 'primary'
            ]
        ]
    ],

    // List
    'list'     => [
        'sorting'    => [
            'mode'        => 1,
            'fields'      => ['name'],
            'flag'        => 1,
            'panelLayout' => 'filter;search,limit',
        ],
        'label'      => [
            'fields' => ['name'],
            'format' => '%s'
        ],
        'operations' => [
            'edit'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_producttype']['edit'],
                'href'  => 'act=edit',
                'icon'  => 'edit.svg'
            ],
            'copy'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_producttype']['copy'],
                'href'  => 'act=copy',
                'icon'  => 'copy.svg',
            ],
            'delete' => [
                'label'      => &$GLOBALS['TL_LANG']['tl_dm_producttype']['delte'],
                'href'       => 'act=delete',
                'icon'       => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
            ],
            'show'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dm_producttype']['show'],
                'href'  => 'act=show',
                'icon'  => 'show.svg'
            ]
        ]
    ],

    // Palettes
    'palettes' => [
        'default' => '{name_legend},name;{details_legend},details;{list_legend},list;{filter_legend},filter;{sorting_legend},sorting'
    ],

    // Fields
    'fields'   => [
        'id'      => [
            'sql' => [
                'type'          => 'integer',
                'unsigned'      => true,
                'autoincrement' => true,
            ]
        ],
        'tstamp'  => [
            'sql' => [
                'type'     => 'integer',
                'unsigned' => true,
                'default'  => 0
            ]
        ],
        'name'    => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dm_producttype']['name'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class'  => 'w50'
            ],
            'sql'       => [
                'type'    => 'string',
                'default' => ''
            ]
        ],
        'details' => [
            'label'            => &$GLOBALS['TL_LANG']['tl_dm_producttype']['details'],
            'exclude'          => true,
            'inputType'        => 'checkboxWizard',
            'options_callback' => ['kehrsolutions_dm.data_container.dm_producttype', 'onGetDetailsFields'],
            'eval'             => [
                'mandatory' => true,
                'multiple'  => true,
                'tl_class'  => 'clr'
            ],
            'sql'              => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ],
        'list'    => [
            'label'            => &$GLOBALS['TL_LANG']['tl_dm_producttype']['list'],
            'exclude'          => true,
            'inputType'        => 'checkboxWizard',
            'options_callback' => ['kehrsolutions_dm.data_container.dm_producttype', 'onGetListFields'],
            'eval'             => [
                'multiple' => true,
                'tl_class' => 'clr'
            ],
            'sql'              => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ],
        'filter'  => [
            'label'            => &$GLOBALS['TL_LANG']['tl_dm_producttype']['filter'],
            'exclude'          => true,
            'inputType'        => 'checkboxWizard',
            'options_callback' => ['kehrsolutions_dm.data_container.dm_producttype', 'onGetFilterFields'],
            'eval'             => [
                'multiple' => true,
                'tl_class' => 'clr'
            ],
            'sql'              => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ],
        'sorting' => [
            'label'            => &$GLOBALS['TL_LANG']['tl_dm_producttype']['sorting'],
            'exclude'          => true,
            'inputType'        => 'checkboxWizard',
            'options_callback' => ['kehrsolutions_dm.data_container.dm_producttype', 'onGetSortingFields'],
            'eval'             => [
                'multiple' => true,
                'tl_class' => 'clr',
            ],
            'sql'              => [
                'type'    => 'blob',
                'notnull' => false
            ]
        ]
    ]
];