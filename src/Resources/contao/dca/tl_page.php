<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Add palettes to tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][] = 'isProductPage';
$GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][] = 'dmSetReaderJumpTo';

\Contao\CoreBundle\DataContainer\PaletteManipulator::create()
    ->addLegend('dm_legend', 'publish_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_BEFORE)
    ->addField('isProductPage', 'dm_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('regular', 'tl_page');

\Contao\CoreBundle\DataContainer\PaletteManipulator::create()
    ->addLegend('dm_legend', 'publish_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_BEFORE)
    ->addField('dmProducerPage', 'dm_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('root', 'tl_page');

/**
 * Add subplattes to tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['subpalettes']['isProductPage']     = 'dmSetReaderJumpTo,dmProductType,dmSale,dmRent,dmImage,dmImgSize';
$GLOBALS['TL_DCA']['tl_page']['subpalettes']['dmSetReaderJumpTo'] = 'dmReaderJumpTo';

/**
 * Add fields to tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['fields']['isProductPage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['isProductPage'],
    'exclude'   => true,
    'filter'    => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'submitOnChange' => true,
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmSetReaderJumpTo'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['dmSetReaderJumpTo'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'submitOnChange' => true,
        'tl_class'       => 'clr',
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmReaderJumpTo'] = [
    'label'      => &$GLOBALS['TL_LANG']['tl_page']['dmReaderJumpTo'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => [
        'mandatory' => true,
        'fieldType' => 'radio',
    ],
    'sql'        => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ],
    'relation'   => [
        'type' => 'hasOne',
        'load' => 'lazy'
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmProductType'] = [
    'label'      => &$GLOBALS['TL_LANG']['tl_page']['dmProductType'],
    'exclude'    => true,
    'inputType'  => 'select',
    'foreignKey' => 'tl_dm_producttype.name',
    'eval'       => [
        'mandatory'          => true,
        'includeBlankOption' => true,
        'chosen'             => true,
        'tl_class'           => 'w50'
    ],
    'relation'   => [
        'type' => 'hasOne',
        'load' => 'eager'
    ],
    'sql'        => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmSale'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['dmSale'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'tl_class' => 'w50 clr',
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmRent'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['dmRent'],
    'exclude'   => true,
    'inputType' => 'checkbox',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => [
        'type'    => 'boolean',
        'default' => 0
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmImage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['dmImage'],
    'exclude'   => true,
    'inputType' => 'fileTree',
    'eval'      => [
        'filesOnly'  => true,
        'fieldType'  => 'radio',
        'extensions' => implode(',', \Contao\System::getContainer()->getParameter('contao.image.valid_extensions')),
        'tl_class'   => 'clr'
    ],
    'sql'       => [
        'type'    => 'binary',
        'length'  => 16,
        'notnull' => false
    ]
];

$GLOBALS['TL_DCA']['tl_page']['fields']['dmProducerPage'] = [
    'label'      => &$GLOBALS['TL_LANG']['tl_page']['dmProducerPage'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => [
        'fieldType' => 'radio'
    ],
    'sql'        => [
        'type'     => 'integer',
        'unsigned' => true,
        'default'  => 0
    ],
    'realtion'   => [
        'type' => 'hasOne',
        'load' => 'lazy'
    ]
];