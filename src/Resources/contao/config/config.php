<?php

/**
 * Device Management bundle for Contao Open Source CMS.
 *
 * @copyright Copyright (c) 2018, Kehr Solutions
 * @author    Kehr Solutions <https://www.kehr-solutions.de>
 * @license   MIT
 */

/**
 * Back end modules
 */
array_insert(
    $GLOBALS['BE_MOD'],
    1,
    [
        'device_management' => [
            'dm_product'     => [
                'tables' => ['tl_dm_product', 'tl_dm_product_category']
            ],
            'dm_producer'    => [
                'tables' => ['tl_dm_producer']
            ],
            'dm_producttype' => [
                'tables' => ['tl_dm_producttype']
            ]
        ]
    ]
);

/**
 * Front end modules
 */
array_insert(
    $GLOBALS['FE_MOD'],
    1,
    [
        'device_management' => [
            'dm_product_list'    => \KehrSolutions\DeviceManagementBundle\Module\ProductList::class,
            'dm_product_reader'  => \KehrSolutions\DeviceManagementBundle\Module\ProductReader::class,
            'dm_category_list'   => \KehrSolutions\DeviceManagementBundle\Module\CategoryList::class,
            'dm_product_filter'  => KehrSolutions\DeviceManagementBundle\Module\ProductFilter::class,
            'dm_product_news'    => KehrSolutions\DeviceManagementBundle\Module\ProductNews::class,
            'dm_producer_reader' => \KehrSolutions\DeviceManagementBundle\Module\ProducerReader::class
        ]
    ]
);

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_dm_product']          = \KehrSolutions\DeviceManagementBundle\Model\DmProductModel::class;
$GLOBALS['TL_MODELS']['tl_dm_product_category'] = \KehrSolutions\DeviceManagementBundle\Model\DmProductCategoryModel::class;
$GLOBALS['TL_MODELS']['tl_dm_producer']         = \KehrSolutions\DeviceManagementBundle\Model\DmProducerModel::class;
$GLOBALS['TL_MODELS']['tl_dm_producttype']      = \KehrSolutions\DeviceManagementBundle\Model\DmProductTypeModel::class;

/**
 * Style sheet
 */
if (TL_MODE == 'BE') {
    $GLOBALS['TL_CSS'][] = 'bundles/devicemanagement/device-management.css';
}

/**
 * Hooks
 */
$GLOBALS['TL_HOOKS']['generateBreadcrumb'][] = ['kehrsolutions.device_management.listener.frontend', 'addProductToBreadcrumb'];
$GLOBALS['TL_HOOKS']['getSearchablePages'][] = ['kehrsolutions.device_management.listener.frontend', 'getSearchablePages'];
$GLOBALS['TL_HOOKS']['replaceInsertTags'][]  = ['kehr_solutions.device_management.listener.insert_tags', 'onReplaceInsertTags'];

/**
 * auto_item keywords
 */
$GLOBALS['TL_AUTO_ITEM'][] = 'product';
$GLOBALS['TL_AUTO_ITEM'][] = 'producer';

/**
 * Notification Center notification types
 */
$GLOBALS['NOTIFICATION_CENTER']['NOTIFICATION_TYPE']['device_management'] = [
    // Type
    'request_form' => [
        'recipients'           => [
            'admin_email'
        ],
        'email_subject'        => [
            'admin_email'
        ],
        'email_text'           => [
            'raw_data',
            'admin_email'
        ],
        'email_html'           => [
            'raw_data',
            'admin_email'
        ],
        'email_sender_name'    => [
            'admin_email',
        ],
        'email_sender_address' => [
            'admin_email'
        ],
        'email_recipient_cc'   => [
            'admin_email'
        ],
        'email_recipient_bcc'  => [
            'admin_email'
        ],
        'email_replyTo'        => [
            'admin_email'
        ],
    ]
];