# Device Management bundle for Contao Open Course CMS

Device Management is a bundle for the [Contao CMS](https://contao.org).

## Copyright
This project has been created and is maintained by [Kehr Solutions](http://www.kehr-solutions.de).